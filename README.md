# Depolymerization

## Project structure

- **wire-gateway**:
  [Taler Wire Gateway HTTP API](https://docs.taler.net/core/api-wire.html)
  server
- **btc-wire**: Taler wire implementation for
  [bitcoincore](https://bitcoincore.org/en/about/) node
- **eth-wire**: Taler wire implementation for
  [go-ethereum](https://geth.ethereum.org/) node
- **uri-pack**: Efficient probabilistic binary encoding for URI
- **db**: Database schemas
- **docs**: Documentation files
- **test**: Test scripts
- **instrumentation**: Instrumentation test tool

## Install from source

Cargo version 1.72.1 or above is required. You can get rustup from your
distribution package manager or from [rustup.rs](https://rustup.rs/).

```
git clone https://git.taler.net/depolymerization.git/
cd depolymerization
make install
```

## Getting started

### Dependencies

Depolymerizer require:

- taler-config from [Taler exchange](https://git.taler.net/exchange.git/)
 ap
- PostgreSQL

#### Bitcoin

[Bitcoind](https://bitcoincore.org/) version 28.0 is expected

#### Ethereum

[Geth](https://geth.ethereum.org/) version 1.13.5 is expected

### Initialization

We expect you to already have written a [configuration](#configuration) and
that PostgreSQL and the node (bitcoind or geth) is configured and running.

We will use `btc-wire` in command, but you can use `eth-wire` interchangeably to
depolymerize Ethereum.

If you want to use a specific configuration file, you can add `-c CONF_PATH` to
every command.

#### Database initialization

```
btc-wire initdb
```

#### Wallet initialization

Depolymerization uses an encrypted wallet, so you need to provide a password in
the environment variable `PASSWORD`.

```
btc-wire initwallet
```

You then need to update your configuration with the provided `PAYTO` config
value.

#### Start depolymerization

```
btc-wire
```

You also need to run the wire gateway `wire-gateway`. It can run from another
user as long it can access the database and the configuration file.

## Configuration

The configuration is based on
[taler.conf](https://docs.taler.net/manpages/taler.conf.5.html).

You can find configurations example for each implementation:

- btc-wire: [minimal](docs/taler-btc-min.conf) or
  [full](docs/taler-btc-full.conf)
- eth-wire: [minimal](docs/taler-eth-min.conf) or
  [full](docs/taler-eth-full.conf)

### Initialization

This is the required configuration for initialization:

```ini
# taler.conf - (fill all ___)
[taler]
# Exchange currency
CURRENCY = ___

[exchange]
# Exchange base url
BASE_URL = ___

[depolymerizer-___]
# Postgres connection URL
DB_URL   = ___
```

`PAYTO` is to be added after wallet initialization.

### Currency

Unlike Bitcoin or Ethereum, we differentiate currencies between networks. The
following currencies are supported:

- Bitcoin currencies (btc-wire):
  - BITCOINBTC for main network
  - TESTBTC for test network
  - DEVBTC for regtest network
- Ethereum currencies (eth-wire):
  - ETHEREUMETH for main network
  - ROPSTENETH for ropsten network
  - DEVETH for dev network

### btc-wire

btc-wire will automatically read the bitcoin configuration file (bitcoin.conf)
to connect to the RPC server. Two flags are mandatory:

- `txindex=1`: btc-wire needs access to transactions not linked to the wire
  wallet
- `maxtxfee=?`: bitcoin transactions fees can exceed taler wire fees, putting
  your wire in bankruptcy. You must specify an acceptable transaction fee cap.

It is also recommended to disable RPC client timeout with `rpcservertimeout=0`
or to set a timeout delay superior than the block delay (e.g
`rpcservertimeout=720`) to prevent recurrent "Broken pipe" errors.

```ini
[depolymerizer-bitcoin]
# Datadir or configuration file path
CONF_PATH    = ~/.bitcoin
# Number of blocks to consider a transactions durable
CONFIRMATION = 6
# Amount to keep when bouncing malformed credit
BOUNCE_FEE   = 0.00001
```

### eth-wire

```ini
[depolymerizer-ethereum]
# Datadir or ipc file path
IPC_PATH     = ~/.ethereum/geth/geth.ipc
# Number of blocks to consider a transactions durable
CONFIRMATION = 37
# Amount to keep when bouncing malformed credit
BOUNCE_FEE   = 0.00001
```

### Wire gateway

```ini
[depolymerizer-___]
# Port on which the server listen
PORT        = 8080
# Path on which the server listen (replace port)
UNIXPATH    =
# HTTP Authentication Scheme (basic or none)
AUTH_METHOD =
# Authentification token (base64 for basic auth)
AUTH_TOKEN  =
```

### Process lifetime

You may want to restart depolymerization processes regularly to improve
stability (ex. fix memory fragmentation). It is possible to configure a lifetime
that triggers a graceful shutdown every time a specific amount of work has been
done.

```ini
[depolymerizer-___]
# Number of requests to serve before gateway shutdown (0 mean never)
HTTP_LIFETIME = 0
# Number of worker's loops before wire implementation shutdown (0 mean never)
WIRE_LIFETIME = 0
```

### Stuck transaction

When we send a transaction with a fee too small, it may not be confirmed in a
timely fashion. To unstuck those transactions, it is possible to replace them
with other transactions with more fees. Depolymerizer can be configured to do
this automatically:

```ini
[depolymerizer-___]
# Delay in seconds before bumping an unconfirmed transaction fee (0 mean never)
BUMP_DELAY = 0
```

## Security

Depolymerizer only use an encrypted wallet and provides an easy way to create
them. It is the administrator's responsibility to back up his wallet and
password.

Only the wire adapter needs to have the password stored in its environment.

## Log format

Wire logs use an ASCII code for transaction logs:

- `<<` for a credit
- `>>` for a debit
- `||` for a bounce

You can have an additional context:

- `bump ` a stuck transaction has been bumped
- `conflict ` a transaction has been canceled by a conflicting blockchain
  transaction
- `recovered ` a successful transaction that we failed to register in the local
  database has been found in the blockchain
- `onchain ` an unknown transaction has been found in the blockchain

## Test

Instrumentation test documentation can be founded in the [instrumentation](instrumentation/README.md) directory.