# btc-wire

btc-wire is taler wire adapter for
[bitcoincore](https://bitcoincore.org/en/about/) node

## Credit metadata format

Starting from a bitcoin payto URI you will have to generate fake segwit
addresses to encode the reserve public key as metadata into a common bitcoin
transaction.

A single segwit address can contain 20B of chosen data. The reserve pub key
being 32B we need two addresses. Therefore, we use two fake addresses consisting of the two key halves prepended
with the same random pattern, except for the first bit which must be 0 for the
first half and 1 for the second one. You must then send a single transaction
with three addresses as recipients.

Segwit addresses are encoded using a bitcoin specific format:
[bech32](https://github.com/bitcoin/bips/blob/master/bip-0173.mediawiki)

As a few lines of code can carry more meaning than many words, you can find a
[simple rust example](src/bin/segwit-demo.rs) in this project and run it with
`make segwit_demo`.

```
Ⅰ - Parse payto uri
Got payto uri: payto://bitcoin/bc1qw508d6qejxtdg4y5r3zarvary0c5xw7kv8f3t4?amount=BTC:0.1&subject=0ZSX8SH0M30KHX8K3Y1DAMVGDQV82XEF9DG1HC4QMQ3QWYT4AF00
Send 0.1 BTC to bc1qw508d6qejxtdg4y5r3zarvary0c5xw7kv8f3t4 with reserve public key 0ZSX8SH0M30KHX8K3Y1DAMVGDQV82XEF9DG1HC4QMQ3QWYT4AF00

Ⅱ - Generate fake segwit addresses
Decode reserve public key: 0x07f3d46620a0c138f5131f82d553706df68175cf4b6018b097a5c77e7b4453c0
Generate random prefix 0x7ea4c272
Split reserve public key in two:
0x07f3d46620a0c138f5131f82d553706d
0xf68175cf4b6018b097a5c77e7b4453c0
Concatenate random prefix with each reserve public key half:
0x7ea4c27207f3d46620a0c138f5131f82d553706d
0x7ea4c272f68175cf4b6018b097a5c77e7b4453c0
Set first bit of the first half:
0x7ea4c27207f3d46620a0c138f5131f82d553706d
Unset first bit of the second half:
0xfea4c272f68175cf4b6018b097a5c77e7b4453c0
Encode each half using bech32 to generate a segwit address:
bc1q06jvyus8702xvg9qcyu02yclst24xurdjvsnqz
bc1ql6jvyuhks96u7jmqrzcf0fw80ea5g57q2eccn6

Ⅲ - Send to many
Send a single bitcoin transaction with the three addresses as recipient as follow:

In bitcoincore wallet use 'Add Recipient' button to add two additional recipient and copy adresses and amounts
bc1qw508d6qejxtdg4y5r3zarvary0c5xw7kv8f3t4 0.10000000 BTC
bc1q06jvyus8702xvg9qcyu02yclst24xurdjvsnqz 0.00000294 BTC
bc1ql6jvyuhks96u7jmqrzcf0fw80ea5g57q2eccn6 0.00000294 BTC

In Electrum wallet paste the following three lines in 'Pay to' field :
bc1qw508d6qejxtdg4y5r3zarvary0c5xw7kv8f3t4,0.10000000
bc1q06jvyus8702xvg9qcyu02yclst24xurdjvsnqz,0.00000294
bc1ql6jvyuhks96u7jmqrzcf0fw80ea5g57q2eccn6,0.00000294
Make sure the amount show 0.10000588 BTC, else you have to change the base unit to BTC
```

## Implementation details

### Stuck transaction

We resolve stuck transactions by always sending replaceable transactions using
[BIP 125](https://github.com/bitcoin/bips/blob/master/bip-0125.mediawiki).

TODO
