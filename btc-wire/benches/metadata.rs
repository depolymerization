/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use btc_wire::segwit::{decode_segwit_msg, encode_segwit_key, rand_addresses};
use common::rand_slice;
use criterion::{criterion_group, criterion_main, Criterion};

fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("SegWit addresses");
    group.bench_function("encode", |b| {
        b.iter_batched(
            || rand_slice(),
            |key| encode_segwit_key("bench", &key),
            criterion::BatchSize::SmallInput,
        );
    });
    group.bench_function("decode", |b| {
        b.iter_batched(
            || rand_addresses("bench", &rand_slice()),
            |addrs| decode_segwit_msg(&addrs),
            criterion::BatchSize::SmallInput,
        );
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
