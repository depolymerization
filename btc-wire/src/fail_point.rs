/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
#[derive(Debug, thiserror::Error)]
#[error("{0}")]
pub struct Injected(&'static str);

/// Inject random failure when 'fail' feature is used
#[allow(unused_variables)]
pub fn fail_point(msg: &'static str, prob: f32) -> Result<(), Injected> {
    #[cfg(feature = "fail")]
    return if common::rand::random::<f32>() < prob {
        Err(Injected(msg))
    } else {
        Ok(())
    };

    Ok(())
}
