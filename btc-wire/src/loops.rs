/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use btc_wire::rpc;
use common::postgres;

use crate::fail_point::Injected;

pub mod analysis;
pub mod watcher;
pub mod worker;

#[derive(Debug, thiserror::Error)]
pub enum LoopError {
    #[error("RPC {0}")]
    Rpc(#[from] rpc::Error),
    #[error("DB {0}")]
    DB(#[from] postgres::Error),
    #[error("Another btc-wire process is running concurrently")]
    Concurrency,
    #[error(transparent)]
    Injected(#[from] Injected),
}

pub type LoopResult<T> = Result<T, LoopError>;
