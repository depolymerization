/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use bitcoin::{hashes::Hash, Network};
use btc_wire::{
    btc_config::{BitcoinConfig, WIRE_WALLET_NAME},
    load_taler_config,
    rpc::{self, auto_rpc_common, auto_rpc_wallet, ErrorCode, Rpc},
    WireState,
};
use clap::Parser;
use common::{
    log::{log::info, OrFail},
    named_spawn, password,
    postgres::NoTls,
    reconnect::auto_reconnect_db,
};
use loops::LoopResult;
use std::path::PathBuf;

use crate::loops::{watcher::watcher, worker::worker};

mod fail_point;
mod loops;
mod sql;

/// Taler wire for bitcoincore
#[derive(clap::Parser, Debug)]
struct Args {
    /// Override default configuration file path
    #[clap(global = true, short, long)]
    config: Option<PathBuf>,
    #[clap(subcommand)]
    init: Option<Init>,
}

#[derive(clap::Subcommand, Debug)]
enum Init {
    /// Initialize database schema and state
    Initdb,
    /// Generate bitcoin wallet and initialize state
    Initwallet,
}

/// TODO support external signer https://github.com/bitcoin/bitcoin/blob/master/doc/external-signer.md

fn main() {
    common::log::init();
    let args = Args::parse();

    match args.init {
        Some(cmd) => init(args.config, cmd).or_fail(|e| format!("{}", e)),
        None => run(args.config),
    }
}

fn init(config: Option<PathBuf>, init: Init) -> LoopResult<()> {
    // Parse taler config
    let (taler_config, path, currency) = load_taler_config(config.as_deref());
    // Connect to database
    let mut db = taler_config.db_config().connect(NoTls)?;
    // Parse bitcoin config
    let btc_conf =
        BitcoinConfig::load(path, currency).or_fail(|e| format!("bitcoin config: {}", e));
    // Connect to bitcoin node
    let mut rpc = Rpc::common(&btc_conf).or_fail(|e| format!("rpc connect: {}", e));
    match init {
        Init::Initdb => {
            let mut tx = db.transaction()?;
            // Load schema
            tx.batch_execute(include_str!("../../db/btc.sql"))?;
            // Init status to true
            tx
                .execute(
                    "INSERT INTO state (name, value) VALUES ('status', $1) ON CONFLICT (name) DO NOTHING",
                    &[&[1u8].as_slice()],
                )?;
            // Init last_hash if not already set
            let genesis_hash = rpc.get_genesis()?;
            tx
                        .execute(
                            "INSERT INTO state (name, value) VALUES ('last_hash', $1) ON CONFLICT (name) DO NOTHING",
                            &[&genesis_hash.as_byte_array().as_slice()],
                        )?;
            tx.commit()?;
            println!("Database initialised");
        }
        Init::Initwallet => {
            // Create wallet
            let passwd = password();
            let created = match rpc.create_wallet(WIRE_WALLET_NAME, &passwd) {
                Err(rpc::Error::RPC {
                    code: ErrorCode::RpcWalletError,
                    ..
                }) => false,
                Err(e) => panic!("{}", e),
                Ok(_) => true,
            };

            rpc.load_wallet(WIRE_WALLET_NAME).ok();

            // Load previous address
            // TODO Use address label instead of the database ?
            let prev_addr = db.query_opt("SELECT value FROM state WHERE name = 'addr'", &[])?;
            let addr = if let Some(row) = prev_addr {
                String::from_utf8(row.get(0)).unwrap()
            } else {
                // Or generate a new one
                let new = Rpc::wallet(&btc_conf, WIRE_WALLET_NAME)
                    .or_fail(|e| format!("rpc connect: {}", e))
                    .gen_addr()?;
                db.execute(
                    "INSERT INTO state (name, value) VALUES ('addr', $1)",
                    &[&new.to_string().as_bytes()],
                )?;
                new.to_string()
            };

            if created {
                println!("Created new wallet");
            } else {
                println!("Found already existing wallet")
            }
            println!("You must backup the generated key file and your chosen password, more info there: https://github.com/bitcoin/bitcoin/blob/master/doc/managing-wallets.md#14-backing-up-the-wallet");
            println!("Public address is {}", &addr);
            println!("Add the following line into taler.conf:");
            println!("[depolymerizer-bitcoin]");
            println!("PAYTO = payto://bitcoin/{}", addr);
        }
    }
    Ok(())
}

fn run(config: Option<PathBuf>) {
    let state = WireState::load_taler_config(config.as_deref());

    #[cfg(feature = "fail")]
    if state.btc_config.network == Network::Regtest {
        common::log::log::warn!("Running with random failures");
    } else {
        common::log::log::error!("Running with random failures is unsuitable for production");
        std::process::exit(1);
    }
    let chain_name = match state.btc_config.network {
        Network::Bitcoin => "main",
        Network::Testnet => "test",
        Network::Signet => "signet",
        Network::Regtest => "regtest",
        _ => unreachable!(),
    };
    info!("Running on {} chain", chain_name);
    // TODO Check wire wallet own config PAYTO address

    let rpc_watcher = auto_rpc_common(state.btc_config.clone());
    let rpc_worker = auto_rpc_wallet(state.btc_config.clone(), WIRE_WALLET_NAME);

    let db_watcher = auto_reconnect_db(state.db_config.clone());
    let db_worker = auto_reconnect_db(state.db_config.clone());
    named_spawn("watcher", move || watcher(rpc_watcher, db_watcher));
    worker(rpc_worker, db_worker, state);
    info!("btc-wire stopped");
}
