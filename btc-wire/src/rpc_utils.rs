/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use std::{path::PathBuf, str::FromStr};

use bitcoin::{Address, Amount, Network};

use crate::rpc::{self, Rpc, Transaction};

/// Default chain dir <https://github.com/bitcoin/bitcoin/blob/master/doc/files.md#data-directory-location>
pub fn chain_dir(network: Network) -> &'static str {
    match network {
        Network::Bitcoin => "main",
        Network::Testnet => "testnet3",
        Network::Regtest => "regtest",
        Network::Signet => "signet",
        _ => unimplemented!(),
    }
}

/// Default rpc port <https://github.com/bitcoin/bitcoin/blob/master/share/examples/bitcoin.conf>
pub fn rpc_port(network: Network) -> u16 {
    match network {
        Network::Bitcoin => 8332,
        Network::Testnet => 18332,
        Network::Regtest => 18443,
        Network::Signet => 38333,
        _ => unimplemented!(),
    }
}

/// Default bitcoin data_dir <https://github.com/bitcoin/bitcoin/blob/master/doc/bitcoin-conf.md>
pub fn default_data_dir() -> PathBuf {
    if cfg!(target_os = "windows") {
        PathBuf::from_str(&std::env::var("APPDATA").unwrap())
            .unwrap()
            .join("Bitcoin")
    } else if cfg!(target_os = "linux") {
        PathBuf::from_str(&std::env::var("HOME").unwrap())
            .unwrap()
            .join(".bitcoin")
    } else if cfg!(target_os = "macos") {
        PathBuf::from_str(&std::env::var("HOME").unwrap())
            .unwrap()
            .join("Library/Application Support/Bitcoin")
    } else {
        unimplemented!("Only windows, linux or macos")
    }
}

/// Minimum dust amount to perform a transaction to a segwit address
pub fn segwit_min_amount() -> Amount {
    // https://github.com/bitcoin/bitcoin/blob/master/src/policy/policy.cpp
    Amount::from_sat(294)
}

/// Get the first sender address from a raw transaction
pub fn sender_address(rpc: &mut Rpc, full: &Transaction) -> rpc::Result<Address> {
    let first = &full.decoded.vin[0];
    let tx = rpc.get_input_output(&first.txid.unwrap())?;
    Ok(tx
        .vout
        .into_iter()
        .find(|it| it.n == first.vout.unwrap())
        .unwrap()
        .script_pub_key
        .address
        .unwrap()
        .assume_checked())
}
