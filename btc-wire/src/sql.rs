/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use bitcoin::{hashes::Hash, Address, Amount as BtcAmount, Txid};
use common::currency::CurrencyBtc;
use common::log::OrFail;
use common::postgres::Row;
use common::sql::{sql_amount, sql_url};

use btc_wire::taler_utils::{btc_payto_addr, taler_to_btc};

/// Bitcoin amount from sql
pub fn sql_btc_amount(row: &Row, idx: usize, currency: CurrencyBtc) -> BtcAmount {
    let amount = sql_amount(row, idx);
    taler_to_btc(&amount, currency).or_fail(|_| {
        format!(
            "Database invariant: expected an bitcoin amount got {}",
            amount
        )
    })
}

/// Bitcoin address from sql
pub fn sql_addr(row: &Row, idx: usize) -> Address {
    let url = sql_url(row, idx);
    btc_payto_addr(&url).or_fail(|_| {
        format!(
            "Database invariant: expected an bitcoin payto url got {}",
            url
        )
    })
}

/// Bitcoin transaction id from sql
pub fn sql_txid(row: &Row, idx: usize) -> Txid {
    let slice: &[u8] = row.get(idx);
    Txid::from_slice(slice).or_fail(|_| {
        format!(
            "Database invariant: expected a transaction if got an array of {}B",
            slice.len()
        )
    })
}
