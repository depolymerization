/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
//! Utils function to convert taler API types to bitcoin API types

use bitcoin::{Address, Amount as BtcAmount, SignedAmount};
use common::api_common::Amount;
use common::currency::CurrencyBtc;
use common::url::Url;
use std::str::FromStr;

/// Generate a payto uri from a btc address
pub fn btc_payto_url(addr: &Address) -> Url {
    Url::from_str(&format!("payto://bitcoin/{}", addr)).unwrap()
}

/// Extract a btc address from a payto uri
pub fn btc_payto_addr(url: &Url) -> Result<Address, String> {
    if url.domain() != Some("bitcoin") {
        return Err(format!(
            "Expected domain 'bitcoin' got '{}'",
            url.domain().unwrap_or_default()
        ));
    }
    let str = url.path().trim_start_matches('/');
    let addr = Address::from_str(str).map_err(|e| e.to_string())?;
    Ok(addr.assume_checked())
}

/// Transform a btc amount into a taler amount
pub fn btc_to_taler(amount: &SignedAmount, currency: CurrencyBtc) -> Amount {
    let unsigned = amount.abs().to_unsigned().unwrap();
    let sat = unsigned.to_sat();
    Amount::new(
        currency.to_str(),
        sat / 100_000_000,
        (sat % 100_000_000) as u32,
    )
}

/// Transform a taler amount into a btc amount
pub fn taler_to_btc(amount: &Amount, currency: CurrencyBtc) -> Result<BtcAmount, String> {
    if amount.currency != currency.to_str() {
        return Err(format!(
            "expected currency {} got {}",
            currency.to_str(),
            amount.currency
        ));
    }

    let sat = amount.value * 100_000_000 + amount.fraction as u64;
    Ok(BtcAmount::from_sat(sat))
}
