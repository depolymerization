/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use std::{
    fmt::Display,
    num::ParseIntError,
    ops::Deref,
    str::FromStr,
    time::{Duration, SystemTime},
};

use serde::{de::Error, ser::SerializeStruct, Deserialize, Deserializer};
use serde_json::Value;

/// <https://docs.taler.net/core/api-common.html#tsref-type-ErrorDetail>
#[derive(Debug, Clone, serde::Serialize)]
pub struct ErrorDetail {
    pub code: i64,
    pub hint: Option<String>,
    pub detail: Option<String>,
    pub parameter: Option<String>,
    pub path: Option<String>,
    pub offset: Option<String>,
    pub index: Option<String>,
    pub object: Option<String>,
    pub currency: Option<String>,
    pub type_expected: Option<String>,
    pub type_actual: Option<String>,
}

/// <https://docs.taler.net/core/api-common.html#tsref-type-Timestamp>
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Timestamp {
    Never,
    Time(SystemTime),
}

#[derive(serde::Serialize, serde::Deserialize)]
struct TimestampImpl {
    t_s: Value,
}

impl Timestamp {
    pub fn now() -> Self {
        Self::Time(SystemTime::now())
    }
}

impl<'de> Deserialize<'de> for Timestamp {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let tmp = TimestampImpl::deserialize(deserializer)?;
        match tmp.t_s {
            Value::Number(s) => {
                if let Some(since_epoch_s) = s.as_u64() {
                    Ok(Self::Time(
                        SystemTime::UNIX_EPOCH + Duration::from_secs(since_epoch_s),
                    ))
                } else {
                    Err(Error::custom("Expected epoch time"))
                }
            }
            Value::String(str) if str == "never" => Ok(Self::Never),
            _ => Err(Error::custom("Expected epoch time or 'never'")),
        }
    }
}

impl serde::Serialize for Timestamp {
    fn serialize<S>(&self, se: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut se_struct = se.serialize_struct("Timestamp", 1)?;
        match self {
            Timestamp::Never => se_struct.serialize_field("t_s", "never")?,
            Timestamp::Time(time) => se_struct.serialize_field(
                "t_s",
                &time
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .unwrap()
                    .as_secs(),
            )?,
        };

        se_struct.end()
    }
}

impl From<SystemTime> for Timestamp {
    fn from(time: SystemTime) -> Self {
        Self::Time(time)
    }
}

/// <https://docs.taler.net/core/api-common.html#tsref-type-SafeUint64>
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, serde::Serialize)]
pub struct SafeU64(u64);

#[derive(Debug, thiserror::Error)]
#[error("{0} unsafe, {0} > (2^53 - 1)")]
pub struct UnsafeUint64(u64);

impl TryFrom<u64> for SafeU64 {
    type Error = UnsafeUint64;

    fn try_from(nb: u64) -> Result<Self, Self::Error> {
        if nb < (1 << 53) - 1 {
            Ok(SafeU64(nb))
        } else {
            Err(UnsafeUint64(nb))
        }
    }
}

impl<'de> Deserialize<'de> for SafeU64 {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        SafeU64::try_from(u64::deserialize(deserializer)?).map_err(D::Error::custom)
    }
}

#[derive(Debug, thiserror::Error)]
pub enum ParseSafeUint64Error {
    #[error(transparent)]
    Unsafe(#[from] UnsafeUint64),
    #[error(transparent)]
    Format(#[from] ParseIntError),
}

/// <https://docs.taler.net/core/api-common.html#tsref-type-Amount>
#[derive(
    Debug, Clone, PartialEq, Eq, serde_with::DeserializeFromStr, serde_with::SerializeDisplay,
)]
pub struct Amount {
    pub currency: String,
    pub value: u64,
    pub fraction: u32,
}

impl Amount {
    pub fn new(currency: impl Into<String>, value: u64, fraction: u32) -> Self {
        Self {
            currency: currency.into(),
            value,
            fraction,
        }
    }
}

#[derive(Debug, thiserror::Error)]
pub enum ParseAmountError {
    #[error("Invalid amount format")]
    FormatAmount,
    #[error("Amount overflow")]
    AmountOverflow,
    #[error(transparent)]
    Format(#[from] ParseIntError),
}

impl FromStr for Amount {
    type Err = ParseAmountError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (currency, amount) = s
            .trim()
            .split_once(':')
            .ok_or(ParseAmountError::FormatAmount)?;
        if currency.len() > 12 {
            return Err(ParseAmountError::FormatAmount);
        }
        let (value, fraction) = amount.split_once('.').unwrap_or((amount, ""));

        let value: u64 = value.parse().map_err(|_| ParseAmountError::FormatAmount)?;
        if value > 1 << 52 {
            return Err(ParseAmountError::FormatAmount);
        }

        if fraction.len() > 8 {
            return Err(ParseAmountError::FormatAmount);
        }
        let fraction: u32 = if fraction.is_empty() {
            0
        } else {
            fraction
                .parse::<u32>()
                .map_err(|_| ParseAmountError::FormatAmount)?
                * 10_u32.pow((8 - fraction.len()) as u32)
        };

        Ok(Self {
            currency: currency.to_string(),
            value,
            fraction,
        })
    }
}

impl Display for Amount {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "{}:{}.{:08}",
            self.currency, self.value, self.fraction
        ))
    }
}

#[test]
fn test_amount() {
    const TALER_AMOUNT_FRAC_BASE: u32 = 100000000;
    // https://git.taler.net/exchange.git/tree/src/util/test_amount.c

    const INVALID_AMOUNTS: [&str; 6] = [
        "EUR:4a",                                                                     // non-numeric,
        "EUR:4.4a",                                                                   // non-numeric
        "EUR:4.a4",                                                                   // non-numeric
        ":4.a4",                                                                      // no currency
        "EUR:4.123456789", // precision to high
        "EUR:1234567890123456789012345678901234567890123456789012345678901234567890", // value to big
    ];

    for str in INVALID_AMOUNTS {
        let amount = Amount::from_str(str);
        assert!(amount.is_err(), "invalid {} got {:?}", str, amount);
    }

    let valid_amounts: Vec<(&str, Amount)> = vec![
        ("EUR:4", Amount::new("EUR", 4, 0)), // without fraction
        (
            "eur:0.02",
            Amount::new("eur", 0, TALER_AMOUNT_FRAC_BASE / 100 * 2),
        ), // leading zero fraction
        (
            " eur:4.12",
            Amount::new("eur", 4, TALER_AMOUNT_FRAC_BASE / 100 * 12),
        ), // leading space and fraction
        (
            " *LOCAL:4444.1000",
            Amount::new("*LOCAL", 4444, TALER_AMOUNT_FRAC_BASE / 10),
        ), // local currency
    ];
    for (str, goal) in valid_amounts {
        let amount = Amount::from_str(str);
        assert!(amount.is_ok(), "Valid {} got {:?}", str, amount);
        assert_eq!(
            *amount.as_ref().unwrap(),
            goal,
            "Expected {:?} got {:?} for {}",
            goal,
            amount,
            str
        );
        let amount = amount.unwrap();
        let str = amount.to_string();
        assert_eq!(amount, Amount::from_str(&str).unwrap(), "{:?}", str);
    }
}

#[derive(Debug, thiserror::Error)]
pub enum ParseBase32Error {
    #[error("Invalid Crockford’s base32 format")]
    Format,
    #[error("Invalid length: expected {0} bytes got {1}")]
    Length(usize, usize),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Base32<const L: usize>([u8; L]);

impl<const L: usize> From<[u8; L]> for Base32<L> {
    fn from(array: [u8; L]) -> Self {
        Self(array)
    }
}

impl<const L: usize> Deref for Base32<L> {
    type Target = [u8; L];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<const L: usize> FromStr for Base32<L> {
    type Err = ParseBase32Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let bytes =
            base32::decode(base32::Alphabet::Crockford, s).ok_or(ParseBase32Error::Format)?;
        let exact: [u8; L] = bytes
            .try_into()
            .map_err(|vec: Vec<u8>| ParseBase32Error::Length(L, vec.len()))?;
        Ok(Self(exact))
    }
}

pub fn base32(bytes: &[u8]) -> String {
    base32::encode(base32::Alphabet::Crockford, bytes)
}

impl<const L: usize> Display for Base32<L> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&base32(&self.0))
    }
}

impl<const L: usize> serde::Serialize for Base32<L> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl<'de, const L: usize> Deserialize<'de> for Base32<L> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        Base32::from_str(&String::deserialize(deserializer)?).map_err(D::Error::custom)
    }
}

/// EdDSA and ECDHE public keys always point on Curve25519
/// and represented  using the standard 256 bits Ed25519 compact format,
/// converted to Crockford Base32.
pub type EddsaPublicKey = Base32<32>;
/// 64-byte hash code
pub type HashCode = Base32<64>;
/// 32-bytes hash code
pub type ShortHashCode = Base32<32>;
