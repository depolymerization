/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

/// Type for the Taler Wire Gateway HTTP API <https://docs.taler.net/core/api-bank-wire.html#taler-wire-gateway-http-api>
use url::Url;

use crate::api_common::{Amount, EddsaPublicKey, HashCode, SafeU64, ShortHashCode, Timestamp};

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct WireConfig {
    pub name: &'static str,
    pub version: &'static str,
    pub currency: &'static str,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct TransferResponse {
    pub timestamp: Timestamp,
    pub row_id: SafeU64,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize, PartialEq, Eq)]
pub struct TransferRequest {
    pub request_uid: HashCode,
    pub amount: Amount,
    pub exchange_base_url: Url,
    pub wtid: ShortHashCode,
    pub credit_account: Url,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct OutgoingHistory {
    pub outgoing_transactions: Vec<OutgoingBankTransaction>,
    pub debit_account: Url,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct OutgoingBankTransaction {
    pub row_id: SafeU64,
    pub date: Timestamp,
    pub amount: Amount,
    pub credit_account: Url,
    pub wtid: ShortHashCode,
    pub exchange_base_url: Url,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct IncomingHistory {
    pub credit_account: Url,
    pub incoming_transactions: Vec<IncomingBankTransaction>,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
#[serde(tag = "type")]
pub enum IncomingBankTransaction {
    #[serde(rename = "RESERVE")]
    IncomingReserveTransaction {
        row_id: SafeU64,
        date: Timestamp,
        amount: Amount,
        debit_account: Url,
        reserve_pub: EddsaPublicKey,
    },
    #[serde(rename = "WAD")]
    IncomingWadTransaction {
        // TODO not yet supported
    },
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct AddIncomingRequest {
    pub amount: Amount,
    pub reserve_pub: EddsaPublicKey,
    pub debit_account: Url,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct AddIncomingResponse {
    pub row_id: SafeU64,
    pub timestamp: Timestamp,
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct HistoryParams {
    pub start: Option<u64>,
    pub delta: i64,
    pub long_pool_ms: Option<u64>,
}
