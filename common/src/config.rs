/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use ini::{Ini, Properties};
use std::{
    path::{Path, PathBuf},
    process::Command,
    str::FromStr,
};
use url::Url;

use crate::{
    currency::Currency,
    log::{fail, OrFail},
};
pub use ini;

// Depolymerizer taler config
pub struct TalerConfig {
    conf: Ini,
    section_name: &'static str,
    pub currency: Currency,
}

impl TalerConfig {
    pub fn load(file: Option<&Path>) -> Self {
        // Load config using taler-config
        let mut cmd = Command::new("taler-config");
        cmd.arg("-d");
        if let Some(path) = file {
            cmd.arg("-c");
            cmd.arg(path);
        }
        let output = cmd
            .output()
            .or_fail(|e| format!("Failed to execute taler-config: {}", e));
        if !output.status.success() {
            fail(format_args!(
                "taler-config failure:\n{}",
                String::from_utf8_lossy(&output.stderr)
            ));
        }
        // Parse ini config
        let conf = ini::Ini::load_from_str(&String::from_utf8_lossy(&output.stdout))
            .or_fail(|e| format!("config format: {}", e));
        let taler = section(&conf, "taler");
        let currency = required(taler, "CURRENCY", string);
        let currency = Currency::from_str(&currency)
            .or_fail(|_| format!("config CURRENCY={} is an unsupported currency", currency));
        let section_name = match currency {
            Currency::BTC(_) => "depolymerizer-bitcoin",
            Currency::ETH(_) => "depolymerizer-ethereum",
        };

        Self {
            conf,
            section_name,
            currency,
        }
    }

    fn section(&self) -> &Properties {
        section(&self.conf, self.section_name)
    }

    fn non_zero_option(&self, name: &str) -> Option<u32> {
        nb(self.section(), name).filter(|nb| *nb != 0)
    }
}

impl TalerConfig {
    /* ----- Common ----- */

    pub fn db_config(&self) -> postgres::Config {
        required(self.section(), "DB_URL", postgres)
    }

    pub fn base_url(&self) -> Url {
        required(section(&self.conf, "exchange"), "BASE_URL", url)
    }

    /* ----- Wire Gateway ----- */

    pub fn payto(&self) -> Url {
        required(self.section(), "PAYTO", url)
    }

    pub fn port(&self) -> u16 {
        nb(self.section(), "PORT").unwrap_or(8080)
    }

    pub fn unix_path(&self) -> Option<PathBuf> {
        path(self.section(), "UNIXPATH")
    }

    pub fn http_lifetime(&self) -> Option<u32> {
        self.non_zero_option("HTTP_LIFETIME")
    }

    pub fn auth_method(&self) -> AuthMethod {
        let section = self.section();
        match required(section, "AUTH_METHOD", string).as_str() {
            "none" => AuthMethod::None,
            "basic" => AuthMethod::Basic(required(section, "AUTH_TOKEN", string)),
            it => fail(format!(
                "unknown config auth method AUTH_METHOD={} expected 'none' or 'basic'",
                it
            )),
        }
    }

    /* ----- Wire Common ----- */

    pub fn confirmation(&self) -> Option<u16> {
        nb(self.section(), "CONFIRMATION")
    }

    pub fn bounce_fee(&self) -> Option<String> {
        string(self.section(), "BOUNCE_FEE")
    }

    pub fn wire_lifetime(&self) -> Option<u32> {
        self.non_zero_option("WIRE_LIFETIME")
    }

    pub fn bump_delay(&self) -> Option<u32> {
        self.non_zero_option("BUMP_DELAY")
    }

    /* ----- Custom ----- */

    pub fn path(&self, name: &str) -> Option<PathBuf> {
        path(self.section(), name)
    }
}

/* ----- Auth Method ----- */

pub enum AuthMethod {
    Basic(String),
    None,
}

/* ----- Helper parsing functions ----- */

pub fn section<'a>(ini: &'a Ini, name: &str) -> &'a Properties {
    ini.section(Some(name))
        .or_fail(|_| format!("missing config section {}", name))
}

pub fn required<T>(
    properties: &Properties,
    name: &str,
    lambda: fn(properties: &Properties, name: &str) -> Option<T>,
) -> T {
    expect_config(lambda(properties, name), name)
}

pub fn expect_config<T>(value: Option<T>, name: &str) -> T {
    value.or_fail(|_| format!("missing config value {}", name))
}

pub fn string(properties: &Properties, name: &str) -> Option<String> {
    properties.get(name).map(|s| s.to_string())
}

pub fn path(properties: &Properties, name: &str) -> Option<PathBuf> {
    properties.get(name).map(|s| {
        PathBuf::from_str(s).or_fail(|_| format!("config {}={} is not a valid path", name, s))
    })
}

pub fn nb<T: FromStr>(properties: &Properties, name: &str) -> Option<T> {
    properties.get(name).map(|s| {
        s.parse()
            .or_fail(|_| format!("config {}={} is not a number", name, s))
    })
}

pub fn url(properties: &Properties, name: &str) -> Option<Url> {
    properties.get(name).map(|s| {
        Url::parse(s).or_fail(|e| format!("config {}={} is not a valid url: {}", name, s, e))
    })
}

pub fn postgres(properties: &Properties, name: &str) -> Option<postgres::Config> {
    properties.get(name).map(|s| {
        postgres::Config::from_str(s)
            .or_fail(|e| format!("config {}={} is not a valid postgres url: {}", name, s, e))
    })
}
