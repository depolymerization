/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use std::str::FromStr;

pub const MAIN_BTC: &str = "BITCOINBTC";
pub const REGTEST_BTC: &str = "TESTBTC";
pub const TESTNET_BTC: &str = "DEVBTC";
pub const MAIN_ETH: &str = "ETHEREUMETH";
pub const GOERLI_ETH: &str = "GOERLIETH";
pub const DEV_ETH: &str = "DEVETH";

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Currency {
    ETH(CurrencyEth),
    BTC(CurrencyBtc),
}

impl Currency {
    pub const fn to_str(&self) -> &'static str {
        match self {
            Currency::BTC(btc) => btc.to_str(),
            Currency::ETH(eth) => eth.to_str(),
        }
    }
}

impl FromStr for Currency {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            MAIN_BTC => Currency::BTC(CurrencyBtc::Main),
            REGTEST_BTC => Currency::BTC(CurrencyBtc::Test),
            TESTNET_BTC => Currency::BTC(CurrencyBtc::Dev),
            MAIN_ETH => Currency::ETH(CurrencyEth::Main),
            GOERLI_ETH => Currency::ETH(CurrencyEth::Goerli),
            DEV_ETH => Currency::ETH(CurrencyEth::Dev),
            _ => return Err(()),
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum CurrencyBtc {
    Main,
    Test,
    Dev,
}

impl CurrencyBtc {
    pub const fn to_str(&self) -> &'static str {
        match self {
            CurrencyBtc::Main => MAIN_BTC,
            CurrencyBtc::Test => REGTEST_BTC,
            CurrencyBtc::Dev => TESTNET_BTC,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum CurrencyEth {
    Main,
    Dev,
    Goerli,
}

impl CurrencyEth {
    pub const fn to_str(&self) -> &'static str {
        match self {
            CurrencyEth::Main => MAIN_ETH,
            CurrencyEth::Goerli => GOERLI_ETH,
            CurrencyEth::Dev => DEV_ETH,
        }
    }
}
