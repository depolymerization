/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use std::{process::exit, thread::JoinHandle};

use ::log::error;
use rand::{rngs::OsRng, RngCore};
use zeroize::Zeroizing;

pub use base32;
pub use postgres;
pub use rand;
pub use url;

pub mod api_common;
pub mod api_wire;
pub mod config;
pub mod currency;
pub mod error_codes;
pub mod log;
pub mod metadata;
pub mod reconnect;
pub mod sql;
pub mod status;

/// Secure random slice generator using getrandom
pub fn rand_slice<const N: usize>() -> [u8; N] {
    let mut slice = [0; N];
    OsRng.fill_bytes(&mut slice);
    slice
}

/// Spawned a named thread
pub fn named_spawn<F, T>(name: impl Into<String>, f: F) -> JoinHandle<T>
where
    F: FnOnce() -> T,
    F: Send + 'static,
    T: Send + 'static,
{
    std::thread::Builder::new()
        .name(name.into())
        .spawn(f)
        .unwrap()
}

/// Read password from env
pub fn password() -> Zeroizing<String> {
    let passwd = std::env::var("PASSWORD").unwrap_or_else(|_| {
        error!("Missing env var PASSWORD");
        exit(1);
    });
    Zeroizing::new(passwd)
}
