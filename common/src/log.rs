/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use flexi_logger::{DeferredNow, LogSpecification, Record};
pub use log;
use log::error;
use std::{fmt::Display, process::exit};

fn custom_format(
    w: &mut dyn std::io::Write,
    now: &mut DeferredNow,
    record: &Record,
) -> Result<(), std::io::Error> {
    write!(
        w,
        "{} {} {}",
        now.format("%+"),
        record.level(),
        &record.args()
    )
}

pub fn init() {
    flexi_logger::Logger::with(LogSpecification::info())
        .log_to_stderr()
        .format(custom_format)
        .start()
        .unwrap();
}

pub trait OrFail<T, E> {
    fn or_fail<F: FnOnce(E) -> String>(self, lambda: F) -> T;
}

impl<T, E> OrFail<T, E> for Result<T, E> {
    fn or_fail<F: FnOnce(E) -> String>(self, lambda: F) -> T {
        self.unwrap_or_else(|e| fail(lambda(e)))
    }
}

impl<T> OrFail<T, ()> for Option<T> {
    fn or_fail<F: FnOnce(()) -> String>(self, lambda: F) -> T {
        self.unwrap_or_else(|| fail(lambda(())))
    }
}

/// Log error message then exit
pub fn fail(msg: impl Display) -> ! {
    error!("{}", msg);
    exit(1);
}
