/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use std::fmt::Debug;

use url::Url;

#[derive(Debug, Clone, thiserror::Error)]
pub enum DecodeErr {
    #[error("Unknown first byte: {0}")]
    UnknownFirstByte(u8),
    #[error(transparent)]
    UriPack(#[from] uri_pack::DecodeErr),
    #[error("Unexpected end of file")]
    UnexpectedEOF,
}

#[derive(Debug, Clone, thiserror::Error)]
pub enum EncodeErr {
    #[error("Unsupported URI scheme {0}")]
    UnsupportedScheme(String),
}

/// Encoded metadata for outgoing transaction
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum OutMetadata {
    Debit { wtid: [u8; 32], url: Url },
    Bounce { bounced: [u8; 32] },
}

// We leave a potential special meaning for u8::MAX
const BOUNCE_BYTE: u8 = u8::MAX - 1;

impl OutMetadata {
    pub fn encode(&self) -> Result<Vec<u8>, EncodeErr> {
        let mut buffer = Vec::new();
        match self {
            OutMetadata::Debit { wtid, url } => {
                let scheme_id = match url.scheme() {
                    "https" => 0,
                    "http" => 1,
                    scheme => return Err(EncodeErr::UnsupportedScheme(scheme.to_string())),
                };
                buffer.push(scheme_id);
                buffer.extend_from_slice(wtid);
                let parts = format!("{}{}", url.domain().unwrap_or(""), url.path());
                let packed = uri_pack::pack_uri(&parts).unwrap();
                buffer.extend_from_slice(&packed);
            }
            OutMetadata::Bounce { bounced } => {
                buffer.push(BOUNCE_BYTE);
                buffer.extend_from_slice(bounced.as_ref());
            }
        }
        Ok(buffer)
    }

    pub fn decode(bytes: &[u8]) -> Result<Self, DecodeErr> {
        if bytes.is_empty() {
            return Err(DecodeErr::UnexpectedEOF);
        }
        match bytes[0] {
            0..=1 => {
                if bytes.len() < 33 {
                    return Err(DecodeErr::UnexpectedEOF);
                }
                let scheme = match bytes[0] {
                    0 => "https",
                    1 => "http",
                    _ => unreachable!(),
                };
                let packed = format!("{}://{}", scheme, uri_pack::unpack_uri(&bytes[33..])?,);
                let url = Url::parse(&packed).unwrap();
                Ok(OutMetadata::Debit {
                    wtid: bytes[1..33].try_into().unwrap(),
                    url,
                })
            }
            BOUNCE_BYTE => {
                if bytes.len() < 33 {
                    return Err(DecodeErr::UnexpectedEOF);
                }
                Ok(OutMetadata::Bounce {
                    bounced: bytes[1..33].try_into().unwrap(),
                })
            }
            unknown => Err(DecodeErr::UnknownFirstByte(unknown)),
        }
    }
}

/// Encoded metadata for incoming transaction
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum InMetadata {
    Credit { reserve_pub: [u8; 32] },
}

impl InMetadata {
    pub fn encode(&self) -> Vec<u8> {
        let mut buffer = Vec::new();
        match self {
            InMetadata::Credit { reserve_pub } => {
                buffer.push(0);
                buffer.extend_from_slice(reserve_pub);
            }
        }
        buffer
    }

    pub fn decode(bytes: &[u8]) -> Result<Self, DecodeErr> {
        if bytes.is_empty() {
            return Err(DecodeErr::UnexpectedEOF);
        }
        match bytes[0] {
            0 => {
                if bytes.len() < 33 {
                    return Err(DecodeErr::UnexpectedEOF);
                }
                Ok(InMetadata::Credit {
                    reserve_pub: bytes[1..33].try_into().unwrap(),
                })
            }
            unknown => Err(DecodeErr::UnknownFirstByte(unknown)),
        }
    }
}

#[cfg(test)]
mod test {

    use url::Url;

    use crate::{
        metadata::{InMetadata, OutMetadata},
        rand_slice,
    };

    #[test]
    fn decode_encode_credit() {
        for _ in 0..4 {
            let metadata = InMetadata::Credit {
                reserve_pub: rand_slice(),
            };
            let encoded = metadata.encode();
            let decoded = InMetadata::decode(&encoded).unwrap();
            assert_eq!(decoded, metadata);
        }
    }

    #[test]
    fn decode_encode_debit() {
        let urls = [
            "https://git.taler.net/",
            "https://git.taler.net/depolymerization.git/",
            "http://git.taler.net/",
            "http://git.taler.net/depolymerization.git/",
        ];
        for url in urls {
            let wtid = rand_slice();
            let url = Url::parse(url).unwrap();
            let metadata = OutMetadata::Debit { wtid, url };
            let encoded = metadata.encode().unwrap();
            let decoded = OutMetadata::decode(&encoded).unwrap();
            assert_eq!(decoded, metadata);
        }
    }

    #[test]
    fn encode_unknown_scheme() {
        let url = "https+wtf://git.taler.net";
        let url = Url::parse(url).unwrap();
        let metadata = OutMetadata::Debit {
            wtid: rand_slice(),
            url,
        };
        let encoded = metadata.encode();
        assert!(encoded.is_err())
    }

    #[test]
    fn decode_encode_bounce() {
        for _ in 0..4 {
            let id: [u8; 32] = rand_slice();
            let info = OutMetadata::Bounce { bounced: id };
            let encoded = info.encode().unwrap();
            let decoded = OutMetadata::decode(&encoded).unwrap();
            assert_eq!(decoded, info);
        }
    }
}
