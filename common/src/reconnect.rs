/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use std::time::Duration;

use exponential_backoff::Backoff;
use log::error;
use postgres::{Client, NoTls};

const MIN_RECONNECT_DELAY: Duration = Duration::from_millis(300);
const MAX_RECONNECT_DELAY: Duration = Duration::from_secs(10);
const VALID_DELAY: Duration = Duration::from_secs(3);

pub struct AutoReconnect<S, C> {
    config: S,
    client: C,
    connect: fn(&S) -> Option<C>,
    check: fn(&mut C) -> bool,
}

impl<S, C> AutoReconnect<S, C> {
    pub fn new(config: S, connect: fn(&S) -> Option<C>, check: fn(&mut C) -> bool) -> Self {
        Self {
            client: Self::connect(&config, connect),
            connect,
            check,
            config,
        }
    }

    /// Create a new client, loop on error
    fn connect(config: &S, connect: fn(&S) -> Option<C>) -> C {
        let backoff = Backoff::new(8, MIN_RECONNECT_DELAY, MAX_RECONNECT_DELAY);
        let mut iter = backoff.iter();
        loop {
            match connect(config) {
                Some(new) => return new,
                None => std::thread::sleep(iter.next().unwrap_or(MAX_RECONNECT_DELAY)),
            }
        }
    }

    /// Get a mutable connection, block until a connection can be established
    pub fn client(&mut self) -> &mut C {
        if (self.check)(&mut self.client) {
            self.client = Self::connect(&self.config, self.connect);
        }
        &mut self.client
    }
}

pub type AutoReconnectDb = AutoReconnect<postgres::Config, Client>;

pub fn auto_reconnect_db(config: postgres::Config) -> AutoReconnectDb {
    AutoReconnect::new(
        config,
        |config| {
            config
                .connect(NoTls)
                .map_err(|err| error!("connect DB: {}", err))
                .ok()
        },
        |client| client.is_valid(VALID_DELAY).is_err(),
    )
}
