/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use std::str::FromStr;

use postgres::Row;
use url::Url;

use crate::{
    api_common::{Amount, SafeU64},
    log::OrFail,
};

/// URL from sql
pub fn sql_url(row: &Row, idx: usize) -> Url {
    let str: &str = row.get(idx);
    Url::from_str(str).or_fail(|_| format!("Database invariant: expected an url got {}", str))
}

/// Ethereum amount from sql
pub fn sql_amount(row: &Row, idx: usize) -> Amount {
    let str: &str = row.get(idx);
    Amount::from_str(str).or_fail(|_| format!("Database invariant: expected an amount got {}", str))
}

/// Byte array from sql
pub fn sql_array<const N: usize>(row: &Row, idx: usize) -> [u8; N] {
    let slice: &[u8] = row.get(idx);
    slice.try_into().or_fail(|_| {
        format!(
            "Database invariant: expected an byte array of {}B for {}B",
            N,
            slice.len()
        )
    })
}

/// Safe safe u64 from sql
pub fn sql_safe_u64(row: &Row, idx: usize) -> SafeU64 {
    let id: i32 = row.get(idx);
    SafeU64::try_from(id as u64).unwrap()
}
