/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
//! Transactions status in database

/// Debit transaction status
///
///           -> Requested  API request
/// Requested -> Sent       Announced to the bitcoin network
/// Sent      -> Requested  Conflicting transaction (reorg)
#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DebitStatus {
    /// Debit have been requested (default)
    Requested = 0,
    /// Debit have been announced to the bitcoin network
    Sent = 1,
}

impl TryFrom<u8> for DebitStatus {
    type Error = ();

    fn try_from(v: u8) -> Result<Self, Self::Error> {
        match v {
            x if x == DebitStatus::Requested as u8 => Ok(DebitStatus::Requested),
            x if x == DebitStatus::Sent as u8 => Ok(DebitStatus::Sent),
            _ => Err(()),
        }
    }
}

/// Bounce transaction status
///
///           -> Requested  Credit in wrong format
/// Requested -> Ignored    Insufficient found
/// Requested -> Sent       Announced to the bitcoin network
/// Sent      -> Requested  Conflicting transaction (reorg)
#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BounceStatus {
    /// Bounce have been requested (default)
    Requested = 0,
    /// Bounce will never be sent (e.g: bounce amount smaller than bounce fee)
    Ignored = 1,
    /// Bounce have been announced to the bitcoin network
    Sent = 2,
}

impl TryFrom<u8> for BounceStatus {
    type Error = ();

    fn try_from(v: u8) -> Result<Self, Self::Error> {
        match v {
            x if x == BounceStatus::Requested as u8 => Ok(BounceStatus::Requested),
            x if x == BounceStatus::Sent as u8 => Ok(BounceStatus::Sent),
            x if x == BounceStatus::Ignored as u8 => Ok(BounceStatus::Ignored),
            _ => Err(()),
        }
    }
}
