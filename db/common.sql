-- Key value state
CREATE TABLE state (
    name TEXT PRIMARY KEY,
    value BYTEA NOT NULL
);

-- Incoming transactions
CREATE TABLE tx_in (
    id SERIAL PRIMARY KEY,
    _date TIMESTAMP NOT NULL DEFAULT now(),
    amount TEXT NOT NULL,
    reserve_pub BYTEA NOT NULL UNIQUE,
    debit_acc TEXT NOT NULL,
    credit_acc TEXT NOT NULL
);

-- Outgoing transactions
CREATE TABLE tx_out (
    id SERIAL PRIMARY KEY,
    _date TIMESTAMP NOT NULL DEFAULT now(),
    amount TEXT NOT NULL,
    wtid BYTEA NOT NULL UNIQUE,
    debit_acc TEXT NOT NULL,
    credit_acc TEXT NOT NULL,
    exchange_url TEXT NOT NULL,
    request_uid BYTEA UNIQUE
);