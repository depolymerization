-- Key value state
CREATE TABLE state (
    name TEXT PRIMARY KEY,
    value BYTEA NOT NULL
);

-- Incoming transactions
CREATE TABLE tx_in (
    id SERIAL PRIMARY KEY,
    _date TIMESTAMP NOT NULL DEFAULT now(),
    amount TEXT NOT NULL,
    reserve_pub BYTEA NOT NULL UNIQUE,
    debit_acc TEXT NOT NULL,
    credit_acc TEXT NOT NULL
);

-- Outgoing transactions
CREATE TABLE tx_out (
    id SERIAL PRIMARY KEY,
    _date TIMESTAMP NOT NULL DEFAULT now(),
    amount TEXT NOT NULL,
    wtid BYTEA NOT NULL UNIQUE,
    debit_acc TEXT NOT NULL,
    credit_acc TEXT NOT NULL,
    exchange_url TEXT NOT NULL,
    request_uid BYTEA UNIQUE,
    status SMALLINT NOT NULL DEFAULT 0,
    txid BYTEA UNIQUE,
    sent TIMESTAMP DEFAULT NULL
);

-- Bounced transaction
CREATE TABLE bounce (
    id SERIAL PRIMARY KEY,
    bounced BYTEA UNIQUE NOT NULL,
    txid BYTEA UNIQUE,
    _date TIMESTAMP NOT NULL DEFAULT now(),
    status SMALLINT NOT NULL DEFAULT 0
)