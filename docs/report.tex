\documentclass[12pt]{article}
\usepackage[tmargin=1in,bmargin=1in,lmargin=1.25in,rmargin=1.25in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{bytefield}
\usepackage{graphics}
\usepackage{parskip}
\usepackage{tikz}
\usepackage{float}
\usepackage{authblk}
\usepackage{acro}

\usetikzlibrary{positioning,fit}

\title{Depolymerization \\ Integrating GNU Taler with blockchain-based cryptocurrencies}
\author{Antoine d'Aligny}
\affil{Bern University of Applied Sciences}
\date{\today}

\DeclareAcronym{DLT}{
  short=DLT,
  long=Distributed Ledger,
}
\DeclareAcronym{DOS}{
  short=DOS,
  long=Denial of service,
}
\DeclareAcronym{BIP}{
  short=BIP,
  long=Bitcoin Improvement Proposal,
}
\DeclareAcronym{RPC}{
  short=RPC,
  long=Remote Procedure Call
}
\DeclareAcronym{API}{
  short=API,
  long=Application programming interface
}
\DeclareAcronym{HTTP}{
  short=HTTP,
  long=Hypertext Transfer Protocol
}

\begin{document}

\maketitle

\clearpage

\subsection*{Abstract}

GNU Taler is an electronic payment system implemented as free software. The goal of this project is to enable payment with blockchain-based cryptocurrencies. 

To achieve this goal, we need to understand how the distributed ledgers of blockchain-based cryptocurrencies work, what their limitations are, and how to mitigate them so that they can serve as a settlement layer. Key challenges include finding a reliable way to write metadata to the blockchain, handling blockchain reorganization, and resolving stuck transactions. 

Blockchain reorganization prevents the finality of blockchain transactions, which is unacceptable for a settlement layer. By using a confirmation delay, we can create a semblance of finality. However, we still have to handle extreme cases where our delay is insufficient by suspending operations and gracefully recovering when possible.

We have created a depolymerizer, which acts as an adapter between GNU Taler and a \ac{DLT}, allowing their use as a settlement layer. By using official DLT clients, and separating Taler specific components from \ac{DLT} specific components, we achieved a simple and maintainable design. 

By proving that blockchains can be used as a settlement layer for GNU Taler, we show that it is not only capable of handling bank money, but also widely used cryptocurrencies.

For cryptocurrencies owners, this integration offers a new solution for instant and low-cost payments that can scale beyond \ac{DLT} limitations while preserving or enhancing privacy.

\subsection*{Acknowledgement}

I would like to kindly thank Christian Grothoff and Emmanuel Benoist (Bern University of Applied Sciences) for their extensive guidance and support during this project.

\clearpage

\tableofcontents

\clearpage

\section{Blockchain-based cryptocurrencies}

A cryptocurrency is a digital currency that relies on code and especially cryptography to secure transactions without a central authority. It is important to understand the basis of how they work and their limitations to understand the integration challenges they pose. For this project, only the two most used one, Bitcoin and Ethereum, were integrated. While other blockchain-based cryptocurrencies may work differently, we are only interested in the specific similarities between these two.

\subsubsection*{Bitcoin}

Bitcoin is the first cryptocurrency to achieve a successful public rollout. Invented by an unknown person or group of people working under the name of Satoshi Nakamoto, it was first mentioned in his white paper published the 28 October 2008 \cite{nakamoto2008bitcoin}. Bitcoin assembled the technological foundation for many of the cryptocurrencies in use today.

\subsubsection*{Ethereum}

Bitcoin is focused on currency transfer transactions and some people wanted to do more with cryptocurrencies, to be able to run smart contracts (programs) with the same guarantee as a currency transfer transaction. Ethereum is built to meet this expectation. After eighteen months of crowdfunding-funded development, Ethereum was launched in July 2015.

\subsection{Blockchain}

At the heart of these currencies is a blockchain. A blockchain is an append-only database composed of a list of linked records called blocks. The content of each block, except the genesis block (the first block), depends on the content of its parent block. This chained dependency enforces the immutability of the database, preventing retroactive modification without altering all subsequent blocks. Cryptocurrency transactions take effect when they are stored inside those blocks.

\subsection{Consensus}

The blockchain itself is just a storage system. To make it a \ac{DLT}, it needs a peer-to-peer network to share its changes. But also a way for participants (nodes) to agree on a single state of the chain, to reach consensus in a network where nodes can be malicious and have an economic interest in deceiving others. There are many ways to create such consensus, but only two of them interest us: proof of work and proof of stake.

\subsubsection*{Proof of work}

This mechanism consists in making the process of appending a block to the blockchain heavy in computation (mining) by requiring brute force hashing for example. Nodes willing to invest their computation power (miners), work to extend the chain they consider the current state. Over time, the longest chain will be the one where the majority of computing power has been invested, which means that it is the one followed by the majority of miners.

This falls short as soon as one node control significantly more computation power than others. This node will not be able to create invalid blocks, because the other nodes will reject them, but will be able to revise transaction history and control the mining of future transactions. This is a majority attack also known as the 51\% attack.

Another problem is that miners are incentivized to invest more and more computing power, which leads to ever-increasing energy consumption and ecological impact. These problems have motivated the search for another mechanism: proof of stake.

\subsubsection*{Proof of stake}

This mechanism replaces the use of computing power with the stacking of money. Validators (who replace miners) stake large sums of money to be allowed to verify blocks and be randomly selected to propose them. In case of malicious behaviour, the stacked money is burned by other validators. 

The effectiveness and security of this mechanism have yet to be proven, but Ethereum plans to adopt it in the summer of 2022.

\subsubsection*{Block time}

Achieving consensus within the peer-to-peer network requires broadcasting the state of the blockchain to most nodes. This coordination takes some time, and we do not want blocks to be mined faster than the network can keep up. An adaptive difficulty algorithm is used to keep the block time close to a constant, by changing the amount of computation required to mine a block. On Ethereum, the block time is about 12 to 14 seconds\footnote{https://ethereum.org/en/developers/docs/blocks/\#block-time}. On Bitcoin, the block time is about 10 minutes. This limitation combined with a block size limit, creates a transaction rate and a blockchain size growth limit that is difficult to change without disrupting the security of the system. 

\subsubsection*{Reorganization}

These decentralized consensus mechanisms lead to the creation of competing blockchain states. When two miners broadcast a new valid block in a short period of time, one part of the network may receive them in a different order than another part. As nodes will follow the first valid block they found, we have a blockchain fork where two different blockchain states are followed in the network (Figure~\ref{fig:reorg}). 

\begin{figure}[H]
    \begin{center}
        \input{figures/reorg.tex}
    \end{center}
    \caption{Blockchain fork}
    \label{fig:reorg}
\end{figure}

Over time, one fork will become longer than the other, and nodes will follow the longest chain. They will replace recent blocks as necessary during a reorganization of the blockchain. Those reorganizations can cause a transaction previously considered mined by a node to no longer be mined. Therefore, blockchain transactions lack finality.

\subsection{Mining incentive}

A minimum amount of mining power is required for the cryptocurrency to work. Without new blocks, there can be no new transaction. The more mining power and diversity (different miners), the harder it is to attack the \ac{DLT}. Since mining is an expensive process, cryptocurrencies must incentivize it by offering rewards to miners.

Rewards are of two kinds:
\begin{itemize}
    \item \textbf{Coin generation} While mining a block, the miner creates money in it that he can use as he pleases. Bitcoins use this type of reward but to reach a constant amount of 21 million bitcoins, this reward decreases over time.
    \item \textbf{Transaction fee} Transactions may contain a tip for the miner to encourage mining. Miners will not mine transactions that cost them money and will mine the most profitable transaction first. Transaction fees are then market-based values, the more transactions there are waiting to be extracted, the higher the fee to be paid to be included in the next block. When a transaction is sent with a fee too small, it may not be mined in a timely fashion. Since transaction fees are unpredictable, transactions sometimes end being stuck for a long time.
\end{itemize}

\subsection{Blockchain-based cryptocurrencies limitations}

As we have seen, blockchain-based cryptocurrencies have certain limitations by design. Block time adds a significant delay to each transaction, and the block size limits the number of possible transactions per second. High transaction fees are required to cover the cost of an expensive mining process. Because the transaction fee is an unpredictable market-based value, transactions can get stuck. Finally, the redundant computational work of all miners has a significant ecological impact. 

\clearpage

\section{GNU Taler}

GNU Taler is an anonymous, taxable payment system implemented as free software and inspired by David Chaum's Ecash \cite{burdges2016enabling} \cite{dold2019gnu}. Taler does not rely on a \ac{DLT}. Taler payments are made using digital form of traditional money anonymized using blind signatures. We provide only a superficial overview of GNU Taler necessary to understand how Depolymerization fits into the system. More information can be found in the project documentation and repository.

\subsection{Overview}

\begin{figure}[hb]
    \begin{center}
        \scalebox{1.2}{\input{figures/taler_arch.tex}}
    \end{center}
    \caption{GNU Taler overview}
\end{figure}


\paragraph*{Exchange}
The exchange is the payment service provider for financial transactions between customers and merchants. The exchange holds money as a reserve for anonymous digital coins.

\paragraph*{Customer}
Customers can withdraw coins from the exchange and store them in their electronic wallets. These coins can then be spent at a merchant.

\paragraph*{Merchant}
Merchants accept coins in exchange for goods and services. They can then deposit these coins at the exchange to receive money in return.

\paragraph*{Auditor}
Auditors monitor the behaviour of the exchanges to ensure that exchanges operate correctly. They are typically operated by financial regulators.

\paragraph*{Settlement layer}
The settlement layer provides finality for wire transfers that allow customers to deposit money and merchant to withdraw money from Taler. It is typically provided by banks. The goal of this project is to use DLT as a settlement layer to support blockchain-based cryptocurrencies.

\subsection{DLT settlement layer}

\begin{figure}[h]
    \begin{center}
        \vspace{0.5em}
        \input{figures/settlement_layer.tex}
    \end{center}
    \caption{DLT settlement layer with Depolymerizer}
    \label{fig:DltSettlement}
\end{figure}

Depolymerizer serves as a middleware between GNU Taler and cryptocurrencies \ac{DLT} (Figure \ref{fig:DltSettlement}). Customers can send money to the Depolymerizer via a credit transaction, to obtain coins that they can use in GNU Taler transactions. Using the Depolymerizer, Taler exchanges can materialize the coins back into the DLT via a debit transaction.

Off-chain transactions have many advantages over on-chain transactions. At the cost of putting trust in exchange operators or auditors, you can have fast and low cost transactions with instant confirmation (ms). GNU Taler offers linear scalability that can solve DLT throughput limitation and, by not relying on Proof of Work, has a much lower ecological impact. GNU Taler does not sacrifice privacy either; it provides privacy when it can and transparency when it has to (regulation: avoid tax evasion and money laundering). 

\clearpage

\section{Resolving blockchain challenges}

Some properties of blockchain-based cryptocurrencies are problematic for their use as a Taler settlement layer. The two main issues are blockchain reorganizations and stuck transactions.

\subsection{Solving chain reorganization}

Taler expects credits to be final. If a credit disappears from the blockchain, an irrevocable debit would no longer be backed by credit. A malicious actor able to rearrange the blockchain at will could use his Taler coins while removing the credit that created those coins and thus get his cryptocurrencies coins back, allowing for double spending. This problem is well known and already mentioned in the Bitcoin white paper \cite{nakamoto2008bitcoin}. The deeper a block is in the blockchain, the harder it is to replace. The depth of a block determines its probability of persistence. The solution is therefore to wait until a block has a certain number of blocks linked to it before considering its transactions as final (Figure \ref{fig:conf_delay}).

\begin{figure}[H]
    \begin{center}
        \vspace{0.5em}
        \input{figures/conf_delay.tex}
    \end{center}
    \caption{Reorganization mitigation using confirmation delay}
    \label{fig:conf_delay}
\end{figure}

The choice of this confirmation delay is a compromise between speed and security. As small reorganizations are common by design, a minimal delay is necessary for proper operation. To be resistant to an adversary with $30\%$ of the total mining power, we need to wait for 6 blocks in Bitcoin ($\approx$ 1h) and 37 blocks in Ethereum ($\approx$ 8min) \cite{gervais2016security}.

Using a confirmation delay does not solve the problem, it only mitigates it. We still have to deal with a potential reorganization that is longer than our delay. When this happens, we look for any missing confirmed credits, if there are none, we can ignore that reorganization (Figure \ref{fig:harmless_reorg}).

\begin{figure}[H]
    \begin{center}
        \vspace{0.5em}
        \input{figures/harmless_reorg.tex}
    \end{center}
    \caption{Harmless reorganization}
    \label{fig:harmless_reorg}
\end{figure}

If confirmed credits have been removed, we will suspend operations. If we are not targeted by an attack and the DLT network is behaving well, the credits will be mined again, and we can wait for them to be confirmed again to resume operations. In the case where the missing credits have been maliciously replaced (Figure \ref{fig:conflict}) we never resume operation.

\begin{figure}[H]
    \begin{center}
        \vspace{0.5em}
        \input{figures/conflict.tex}
    \end{center}
    \caption{Reorganization with conflicting transaction}
    \label{fig:conflict}
\end{figure}

\subsection{Adaptive confirmation}

If we experience a reorganization once, it is dangerously likely that another one of the same magnitudes will occur again. Depolymerizer learns from reorganizations by increasing its confirmation time (Figure \ref{fig:analysis}). Since we understand that a longer delay can also be counterproductive, we also want to limit the delay adjustment.

\begin{figure}[H]
    \begin{center}
        \vspace{0.5em}
        \input{figures/analysis.tex}
    \end{center}
    \caption{Adaptive confirmation}
    \label{fig:analysis}
\end{figure}

\subsection{Solving stuck transaction}

We know that transactions can get stuck for a long time, which can be problematic when we are expecting transactions to be executed in a timely manner. Depolymerizer keeps track of pending transactions and finds those that are taking an excessive amount of time to mine. Then, it increases the fee for these transactions to bump their mining priority. Since the process of replacing transactions is expensive, this feature is optional and configurable.

\clearpage

\section{Metadata} \label{metadata}

GNU Taler need additional metadata to link a wallet to credits and allow merchants to link deposits to debits. Metadata need to be stored alongside transactions in the blockchain, so it is possible to recover the full transaction history of any depolymerizer from it.

\subsection{Metadata format}

The goal of our metadata format is to be simple to parse and versioned for future extensions.

\subsubsection*{Incoming transaction}

Incoming transaction metadata contains a reserve public key, which is a 32B hash of a public key. We just prepend a versioning byte to allow future extension.

\begin{figure}[h]
    \begin{center}
        \begin{bytefield}{33}
            \bitheader{0,1,32} \\
            \bitbox{1}{\tiny 0} & \bitbox{32}{Key Hash}
        \end{bytefield}
    \end{center}
    \caption{Incoming metadata format}
\end{figure}

\subsubsection*{Outgoing transaction}

Outgoing transactions can be of two types: credit or bounce. Credit metadata contains the wire transfer ID which is a 32B hash and the exchange base URL which is of variable size and encoded using uri-packing (Section \ref{uri-packing}). Bounce metadata contains the bounced transaction ID which is implementation-dependent but is 32B for Bitcoin and Ethereum. A prepended versioning byte differentiates the two types, 0 being a credit and 254 a bounce.


\begin{figure}[h]
    \begin{center}
        \begin{bytefield}[rightcurly=., rightcurlyspace=0pt]{33}
            \bitheader{0,1,32,33}  \\
            \begin{rightwordgroup}{Credit}
                \bitbox{1}{\tiny 0} & \bitbox{32}{Transfer ID} & \bitbox{10}{Base URL}
            \end{rightwordgroup} \\ \\
            \begin{rightwordgroup}{Bounce}
                \bitbox{1}{\tiny \rotatebox{90}{254}} & \bitbox{32}{Transaction ID}
            \end{rightwordgroup}
        \end{bytefield}
    \end{center}
    \caption{Outgoing metadata format}
\end{figure}

\subsection{Bitcoin}

There are many documented ways to encode metadata in a bitcoin transaction \cite{bartoletti2019journey}. In the early days of this cryptocurrency, users abused the transaction format to store metadata, and some of these techniques result in ecosystem pollution. For example, bitcoin clients keep a list of all addresses that currently have money to spend (UTXO), with fake addresses wasting space there indefinitely. To allow storage of a reasonable amount of metadata without polluting the blockchain, a new opcode script OP\_RETURN was created, allowing up to 80B of storage per transaction.

Debits are performed from our code using OP\_RETURN to store metadata, but credits are done from common wallet clients, and they do not yet support custom metadata. We had to find another format using fake addresses.

We use the latest address type, segwit addresses \cite{BIP173}, where we can embed 20B of chosen data. Since the reserve pub key is a 32B hash, we need two addresses. Therefore, we use two fake addresses each containing one half of the key prepended by a common random prefix, except for the first bit, which must be 0 for the first half and 1 for the second. We then send a single transaction with three addresses (the exchange address and the two fake) as recipients.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \draw[dotted,thick] (-6.33,1.13) -- (-6.33,0.185);
        \draw[dotted,thick] (-5,1.13) -- (4.3,0.185);
        \node {
            \begin{bytefield}[rightcurly=., rightcurlyspace=0pt]{32}
                \bitheader{0,3,4,19} \\
                \begin{rightwordgroup}{Address}
                    \bitbox{4}{ID} & \bitbox{16}{Half}
                \end{rightwordgroup} \\ \\
                \bitheader{0,1,31} \\
                \begin{rightwordgroup}{First ID}
                    \bitbox{1}{\tiny 0} & \bitbox{31}{Common random prefix}
                \end{rightwordgroup} \\ 
                \bitbox[]{32}{or} \\
                \begin{rightwordgroup}{Second ID}
                    \bitbox{1}{\tiny 1} & \bitbox{31}{Common random prefix}
                \end{rightwordgroup}
            \end{bytefield}
        };
    \end{tikzpicture}
    \caption{Outgoing metadata format}
\end{figure}

Having a common random prefix allows us to distinguish real addresses from fake ones, since it is very unlikely that two addresses share the same 32 bits. Since the Bitcoin client randomizes the order of the addresses for privacy reasons, the first bit allows us to distinguish the first half of the key from the second.

\subsection{Ethereum}

Ethereum is designed around the concept of smart contracts. Logging inside a smart contract is the recommended way to add metadata\footnote{https://ethereum.org/en/developers/docs/smart-contracts/anatomy/\#events-and-logs}, but it is expensive (additional storage and execution costs) and adds an avoidable attack surface. We chose to use the transaction field typically used to call smart contracts to store our raw metadata.

\subsection{Friendly behaviour on format error}

When we receive a transaction without any metadata or with an incompatible format (bogus wallet), we want to return the money to its owner (bounce). However, this can be dangerous because it creates a potential attack loophole as anyone can now make Depolymerizer do a transaction, by sending a malformed transaction. Depolymerizer takes a bounce fee to make a potential \ac{DOS} attack too costly. There is another issue, making a transaction has a cost, and if the transaction fee is higher than our bounce fee, malicious bounces could bankrupt us. We also need to charge the recipient the transaction fee to ensure we never lose money on a bounce.

\clearpage

\section{Architecture}

Each cryptocurrency uses a different \ac{DLT} with its own format and rules, which evolve over time. We do not want to manage the \ac{DLT} logic ourselves, nor do we want to rely on third-party dependencies to implement their support properly and be maintained. The simplest solution is to rely on the official clients and communicate with them via \ac{RPC}.

\begin{figure}[h]
    \begin{center}
        \input{figures/depolymerizer_arch.tex}
    \end{center}
    \caption{Depolymerizer architecture}
\end{figure}

While some parts of Depolymerizer are \ac{DLT} specific, much of the logic is common, and we want to reuse it. We have a Wire Gateway component that implements the Taler \ac{HTTP} \ac{API} to enable communication with Taler exchanges. Each supported cryptocurrency has its specific adapter to communicate with the official full node client via \ac{RPC}. The Wire Gateway module and the \ac{DLT} adapter use a common database to store transactions and communicate with notifications.

\subsection{DLT adapter}

The DTL adapter uses an event-based architecture with three distinct loops.

\paragraph*{Block watcher}

The watcher loop looks for new incoming blocks and notifies the other loops of their arrival.

\paragraph*{Analysis}

The analysis loop waits for new blocks and then analyzes the behavior of the DLT network. If a dangerous reorganization occurs, it is responsible for updating the confirmation delay. 

\paragraph*{Worker}

The worker loop waits for new blocks or transaction requests (from the Wire Gateway \ac{API}). When one of these events occurs, it first reconciles the local database with the \ac{DLT}, then triggers requested debits, re-issues blocked debits and bounces malformed credits.

\subsection{Worker loop in detail}

\subsubsection*{DLT reconciliation}

During a \ac{DLT} reconciliation, we first list all new transactions and any transactions that have been removed in a reorganization since the last reconciliation. If any previously confirmed debits have been removed without being reinserted into another block, we notify the Wire Gateway to cease activity and wait for the next block in hopes of recovering them. All newly confirmed debits and successful credits are registered in the database.

\subsubsection*{Reconciliation inconsistency}

When we issue a transaction (debit or bounce), it is possible for the database or \ac{DLT} \ac{RPC} request to fail. Since a \ac{DLT} \ac{RPC} request error does not mean that the cryptocurrency transaction was not successful, and since the database may not record a successful transaction, it is possible to have an inconsistency between the DLT and the database where a successful transaction is not recorded as such. This is very problematic because we must perform each transaction only once.

This is also problematic because, even if we used a status machine state in the database to detect this inconsistency, the only way to resolve it is to make another \ac{DLT} reconciliation, which is slow and does not play well with database locking.

Since we know that blockchain-based cryptocurrencies have low throughput, we do not need parallel worker loops to stay synchronized. Therefore, we can use a cross-process locking mechanism to ensure that only one working loop is running at a time. Then, when a database or \ac{DLT} request error occurs, we can restart the loop, which will start by performing a \ac{DLT} reconciliation that will recover all successful unregistered transactions.

\clearpage

\section{Implementation specific issues}

\subsection*{Ethereum amount precision}

The Taler amount format comes from RFC 8905 \cite{RFC8905}. It allows up to $2^{53}$ unit and 8 decimal digits. This format is perfectly suited for Bitcoin where the maximal amount is 21 million bitcoins and the minimum amount is the satoshi, one satoshi being worth $10^{8}$ bitcoin. However, the minimum amount of Ethereum is the wei, with one ether being worth $10^{18}$ wei. The amount of ether in circulation continues to grow without a cap, with over 119,000,000 ether in circulation at the time of writing those lines. Therefore, it is not possible to represent all Ethereum amounts with the current format.

A standard Ethereum transaction requires 21 000 units of gas\footnote{https://ethereum.org/en/developers/docs/gas/\#post-london}. The average gas price is currently around 30 Gwei. Therefore, a standard transaction cost about $63.10^{18}$ wei in transaction fees. Since the transaction fee is so high, even if we truncate Ethereum value to $10^{-8}$ eth ($10^{10}$ wei), we can still represent any amount you can send without losing money on the transaction fee. In Depolymerizer, all Ethereum amounts are truncated as such.

\subsection*{Replaceable bitcoin transaction}

When some merchants wanted to allow instant payments with Bitcoin, they chose to consider a transaction final when it is announced. Although wrong, this choice works most of the time because many nodes do not accept conflicting transactions in their mempool, making it difficult to replace or cancel a transaction that has already been announced.

This becomes problematic when you want to make a legitimate replacement, to unstuck a transaction by increasing its transaction fee for example. At the same time, it is dangerous to give an easy way to attackers and scammers to change the content of announced transaction.

We use the solution has been adopted in \ac{BIP} 125 \cite{BIP125}, adding the possibility to encode the replaceability of a bitcoin transaction at its creation. It can thus be replaced by a new transaction within certain rules: you cannot send less money to existing recipients, and you must pay a replacement fee as a countermeasure to a \ac{DOS} attack.

\clearpage

\section{URI packing, a compression side quest}\label{uri-packing}

\subsection*{The need for compact URI}

As discussed previously in section \ref{metadata}, storing metadata in blockchain is
expensive and limited. Therefore, we want our metadata to be as small as possible.

\noindent
Transactions metadata are composed of three parts:
\begin{itemize}
    \item Version and identity metadata ($\sim$ 1B)
    \item Reserve public key or wire transfer ID (32B)
    \item Base URL (debit only, variable)
\end{itemize}

The only variable, and so problematic, part is the base URL. Those URLs have some
property in common, they always use a few different scheme (http or https) and
are composed of a domain and a small path.

We would normally encode the URL using ASCII, but we knew only a few ASCII characters are actually used, and we can take advantage of that.

\subsection*{5 or 11 encoding}

Our idea is to encode the most commonly used characters using five bits, and the remaining characters using eleven bits. As ASCII characters are seven bits wide and are commonly encoded using height, we gain on size if more than half of the characters composing the URI are encodable using fewer bits (Table~\ref{table:uri-packing}). You can find the detailed encoding table in appending \ref{5-11}.

\begin{table}[h]
    \centering
    \begin{tabular}{ll}
        value    & encoding                                    \\
        \hline
        0..30    & common character: a-z . / - \%              \\
        30 0..64 & extended character, remaining graphic ascii \\
        31       & end of encoded string                       \\
    \end{tabular}
    \caption{URI packing encoding}
    \label{table:uri-packing}
\end{table}

Using this encoding format on all domains on the
majestic-million\footnote{https://majestic.com/reports/majestic-million}
database, $98.77\%$ of the domain name where smaller, going from an average encoded size of 14B in ASCII to 10B using our format.

\subsection*{Uri in metadata}

To further optimize metadata size we chose to encode the URI scheme into the
version and identity metadata byte and the remaining domain and path using our
custom format.

For example, for bitcoin the maximum amount of data than is accepted in
OP\_RETURN is currently 80 bytes, leaving us 47 bytes to store the URI. With our
encoding we can encode in the best case 74 characters instead of 47 which is more than enough for our use case.

\clearpage

\section{Taler Wire Gateway HTTP API}

Taler is a modular project where each module communicates through \ac{HTTP} \ac{API}. The Wire Gateway \ac{API} allows the exchange to communicate to wire adaptors. The Wire Gateway module allow Depolymerizer to communicate with Taler exchanges. As the \ac{API} can be exposed on the Internet it has to be resistant to most of the known attacks.

\subsection*{HTTP Authentication}

The wire \ac{API} only supports the Basic \ac{HTTP} Authentication method and it has to be optional. Making it optional can lead to security issues by misconfiguration. If the default behavior in case of missing configuration is to deactivate authentication, a typo could lead to an exposed \ac{API}. We made the authentication method configuration mandatory to make its deactivation explicit.

\subsection*{OOM DOS}

A common Denial Of Service attack consists of sending many requests with huge bodies to saturate a server memory and, in the worst case, create an Out Of Memory error. To be resilient against such attacks we only read body after request authentication, to prevent any person without authorization to access the \ac{API} to perform such attacks.

Then we chose an aggressive memory budget of 4kB, as all request bodies should be very small, and we only read and parse them under this budget. In the case of compressed bodies, we also apply this budget to the decompression process to protect ourselves against decompression bombs.

\subsection*{Testing}

The Taler exchange has a taler-exchange-wire-gateway-client CLI that allowed me to test that my implementation not only conforms to the \ac{API} documentation but also with how the official client handles it. I found confusion in the documentation where it was specified that timestamp should consist of time in milliseconds since epoch, but the client will reject timestamps that are not rounded to second.

\clearpage

\section{Conclusion}

\subsection*{Summary}

We have shown that is it possible to use Bitcoin and Ethereum \ac{DLT} as a settlement layer for GNU Taler enabling payment with blockchain-based cryptocurrencies using Depolymerization. Depolymerization act as a middleware between Taler exchange and a \ac{DLT} mitigating their inconsistency especially chain reorganization and stuck transactions.

In this project, we have not addressed the legal challenges of running GNU Taler with Depolymerization. In some countries, the use of cryptocurrencies is prohibited or restricted, and in most countries, payment systems are regulated and may require a licence. We therefore advise against deploying it without checking that you are authorized to do so.

Depolymerization has many attractive features for cryptocurrency users at one cost, the centralization of trust in the exchange. This problem is partially solved with the use of auditors that federate trust. In contrast, blockchain-based cryptocurrencies are generally public, with their entire state subject to public scrutiny. For GNU Taler to convince users who expect this level of openness, we would have to find a way to make enough information public that any client could do the auditors' job.


\subsection*{Future work}

\subsubsection*{Paying for reorganization loss}

When a reorganization removes a confirmed credit indefinitely (conflicting transaction) we suspend operation indefinitely. We could allow exchange administrators to pay for the missing transactions to resume operations, as sometimes the loss caused by the non-functioning Depolymerizer exceeds the cost of the missing transactions.

\subsubsection*{Smarter analysis}

More intelligent analysis of network behaviors can be performed to tailor the confirmation time to an estimated risk factor. Brute force attacks on a DLT are very expensive, and one can expect that attackers have an economic incentive to attack through double spending for example. When we receive a large credit or when a considerable amount of currency is exchanged on the network, this can be the preparation for such attacks. We could monitor these indicators, and we could apply a longer temporary delay to make the attacks harder to sustain.

\clearpage

\bibliographystyle{alpha}
\bibliography{literature}

\clearpage

\printacronyms

\clearpage

\appendix

\section*{5-11 encoding table}\label{5-11}

\input{tables/5-11.tex}

\end{document}
