/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use common::log::log::warn;

use super::LoopResult;

/// Analyse blockchain behavior and adapt confirmations in real time
pub fn analysis(fork: u32, current: u32, max: u32) -> LoopResult<u32> {
    // If new fork is bigger than what current confirmation delay protect against
    if fork >= current {
        // Limit confirmation growth
        let new_conf = fork.saturating_add(1).min(max);
        warn!(
            "analysis: found dangerous fork of {} blocks, adapt confirmation to {} blocks capped at {}, you should update taler.conf",
            fork, new_conf, max
        );
        return Ok(new_conf);
    }
    Ok(current)
}
