use std::time::Duration;

/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use common::{log::log::error, reconnect::AutoReconnectDb};
use eth_wire::rpc::AutoRpcCommon;

use super::LoopResult;

/// Wait for new block and notify arrival with postgreSQL notifications
pub fn watcher(mut rpc: AutoRpcCommon, mut db: AutoReconnectDb) {
    loop {
        let rpc = rpc.client();
        let db = db.client();

        let result: LoopResult<()> = (|| {
            let mut notifier = rpc.subscribe_new_head()?;
            loop {
                db.execute("NOTIFY new_block", &[])?;
                notifier.next()?;
            }
        })();
        if let Err(e) = result {
            error!("watcher: {}", e);
            std::thread::sleep(Duration::from_secs(5));
        }
    }
}
