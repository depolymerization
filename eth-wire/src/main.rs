/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use std::path::PathBuf;

use clap::Parser;
use common::{
    log::{log::info, OrFail},
    named_spawn, password,
    postgres::NoTls,
    reconnect::auto_reconnect_db,
};
use eth_wire::{
    load_taler_config,
    rpc::{auto_rpc_common, auto_rpc_wallet, Rpc, RpcClient},
    SyncState, WireState,
};
use ethereum_types::H160;
use loops::{watcher::watcher, worker::worker, LoopResult};

mod fail_point;
mod loops;
mod sql;

/// Taler wire for geth
#[derive(clap::Parser, Debug)]
struct Args {
    /// Override default configuration file path
    #[clap(global = true, short, long)]
    config: Option<PathBuf>,
    #[clap(subcommand)]
    init: Option<Init>,
}

#[derive(clap::Subcommand, Debug)]
enum Init {
    /// Initialize database schema and state
    Initdb,
    /// Generate ethereum wallet and initialize state
    Initwallet,
}

fn main() {
    common::log::init();
    let args = Args::parse();

    match args.init {
        Some(cmd) => init(args.config, cmd).or_fail(|e| format!("{}", e)),
        None => run(args.config),
    }
}

fn init(config: Option<PathBuf>, init: Init) -> LoopResult<()> {
    // Parse taler config
    let (taler_config, ipc_path, _) = load_taler_config(config.as_deref());
    // Connect to database
    let mut db = taler_config.db_config().connect(NoTls)?;
    // Connect to ethereum node
    let mut rpc = Rpc::new(ipc_path).or_fail(|e| format!("rpc connect: {}", e));

    match init {
        Init::Initdb => {
            let mut tx = db.transaction()?;
            // Load schema
            tx.batch_execute(include_str!("../../db/eth.sql"))?;
            // Init status to true
            tx
              .execute(
                  "INSERT INTO state (name, value) VALUES ('status', $1) ON CONFLICT (name) DO NOTHING",
                  &[&[1u8].as_ref()],
              )?;
            // Init sync if not already set
            let block = rpc.earliest_block()?;
            let state = SyncState {
                tip_hash: block.hash.unwrap(),
                tip_height: block.number.unwrap(),
                conf_height: block.number.unwrap(),
            };
            tx.execute(
                "INSERT INTO state (name, value) VALUES ('sync', $1) ON CONFLICT (name) DO NOTHING",
                &[&state.to_bytes().as_ref()],
            )?;
            tx.commit()?;
            println!("Database initialised");
        }
        Init::Initwallet => {
            // Skip previous blocks
            let block = rpc.latest_block()?;
            let state = SyncState {
                tip_hash: block.hash.unwrap(),
                tip_height: block.number.unwrap(),
                conf_height: block.number.unwrap(),
            };
            let prev_addr = db.query_opt("SELECT value FROM state WHERE name = 'addr'", &[])?;
            let (addr, created) = if let Some(row) = prev_addr {
                (H160::from_slice(row.get(0)), false)
            } else {
                // Or generate a new one
                let passwd = password();
                let new = rpc.new_account(&passwd)?;
                db.execute(
                    "INSERT INTO state (name, value) VALUES ('addr', $1)",
                    &[&new.as_bytes()],
                )?;
                let nb_row = db.execute(
                    "UPDATE state SET value=$1 WHERE name='sync'",
                    &[&state.to_bytes().as_ref()],
                )?;
                if nb_row > 0 {
                    println!("Skipped {} previous block", state.conf_height);
                }
                (new, true)
            };

            if created {
                println!("Created new wallet");
            } else {
                println!("Found already existing wallet")
            };

            let addr = hex::encode(addr.as_bytes());
            println!("You must backup the generated key file and your chosen password, more info there: https://geth.ethereum.org/docs/install-and-build/backup-restore");
            println!("Public address is {}", &addr);
            println!("Add the following line into taler.conf:");
            println!("[depolymerizer-ethereum]");
            println!("PAYTO = payto://ethereum/{}", addr);
        }
    }
    Ok(())
}

fn run(config: Option<PathBuf>) {
    let state = WireState::load_taler_config(config.as_deref());

    let rpc_worker = auto_rpc_wallet(state.ipc_path.clone(), state.address);
    let rpc_watcher = auto_rpc_common(state.ipc_path.clone());

    let db_watcher = auto_reconnect_db(state.db_config.clone());
    let db_worker = auto_reconnect_db(state.db_config.clone());

    named_spawn("watcher", move || watcher(rpc_watcher, db_watcher));
    worker(rpc_worker, db_worker, state);
    info!("eth-wire stopped");
}
