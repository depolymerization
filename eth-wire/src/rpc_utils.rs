/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use std::{path::PathBuf, str::FromStr};

/// Default geth data_dir <https://geth.ethereum.org/docs/install-and-build/backup-restore#data-directory>
pub fn default_data_dir() -> PathBuf {
    if cfg!(target_os = "windows") {
        PathBuf::from_str(&std::env::var("APPDATA").unwrap())
            .unwrap()
            .join("Ethereum")
    } else if cfg!(target_os = "linux") {
        PathBuf::from_str(&std::env::var("HOME").unwrap())
            .unwrap()
            .join(".ethereum")
    } else if cfg!(target_os = "macos") {
        PathBuf::from_str(&std::env::var("HOME").unwrap())
            .unwrap()
            .join("Library/Ethereum")
    } else {
        unimplemented!("Only windows, linux or macos")
    }
}
