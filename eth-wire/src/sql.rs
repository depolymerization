/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use common::{
    currency::CurrencyEth,
    log::OrFail,
    postgres::Row,
    sql::{sql_amount, sql_array, sql_url},
};
use eth_wire::taler_util::{eth_payto_addr, taler_to_eth};
use ethereum_types::{H160, H256, U256};

/// Ethereum amount from sql
pub fn sql_eth_amount(row: &Row, idx: usize, currency: CurrencyEth) -> U256 {
    let amount = sql_amount(row, idx);
    taler_to_eth(&amount, currency).or_fail(|_| {
        format!(
            "Database invariant: expected an ethereum amount got {}",
            amount
        )
    })
}

/// Ethereum address from sql
pub fn sql_addr(row: &Row, idx: usize) -> H160 {
    let url = sql_url(row, idx);
    eth_payto_addr(&url).or_fail(|_| {
        format!(
            "Database invariant: expected an ethereum payto url got {}",
            url
        )
    })
}

/// Ethereum hash from sql
pub fn sql_hash(row: &Row, idx: usize) -> H256 {
    let array: [u8; 32] = sql_array(row, idx);
    H256::from_slice(&array)
}
