/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use std::str::FromStr;

use common::{api_common::Amount, currency::CurrencyEth, url::Url};
use ethereum_types::{Address, U256};

pub const WEI: u64 = 1_000_000_000_000_000_000;
pub const TRUNC: u64 = 10_000_000_000;

/// Generate a payto uri from an eth address
pub fn eth_payto_url(addr: &Address) -> Url {
    Url::from_str(&format!(
        "payto://ethereum/{}",
        hex::encode(addr.as_bytes())
    ))
    .unwrap()
}

/// Extract an eth address from a payto uri
pub fn eth_payto_addr(url: &Url) -> Result<Address, String> {
    if url.domain() != Some("ethereum") {
        return Err(format!(
            "Expected domain 'ethereum' got '{}'",
            url.domain().unwrap_or_default()
        ));
    }
    let str = url.path().trim_start_matches('/');
    Address::from_str(str).map_err(|e| e.to_string())
}

/// Transform a eth amount into a taler amount
pub fn eth_to_taler(amount: &U256, currency: CurrencyEth) -> Amount {
    Amount::new(
        currency.to_str(),
        (amount / WEI).as_u64(),
        ((amount % WEI) / TRUNC).as_u32(),
    )
}

/// Transform a eth amount into a btc amount
pub fn taler_to_eth(amount: &Amount, currency: CurrencyEth) -> Result<U256, String> {
    if amount.currency != currency.to_str() {
        return Err(format!(
            "expected currency {} got {}",
            currency.to_str(),
            amount.currency
        ));
    }

    Ok(U256::from(amount.value) * WEI + U256::from(amount.fraction) * TRUNC)
}
