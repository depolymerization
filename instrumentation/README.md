# Depolymerizer instrumentation test

## Install

`cargo install --path instrumentation`

## Offline local tests

Local tests require additional binaries. The following binaries must be in the
local user PATH:

- `pg_ctl` and `psql` from PostgreSQL
- `geth` from [go-ethereum](https://geth.ethereum.org/downloads/)
- `bitcoind` and `bitcoin-cli` from
  [bitcoincore](https://bitcoincore.org/en/download/)
- `taler-config` and `taler-exchange-wire-gateway-client` from the
  [Taler exchange ](https://git.taler.net/exchange.git/)

You can use the [prepare](script/prepare.sh) script to download and extract
blockchain binaries and find the path of the local Postgres installation.
However, taler binaries need to be compiled from source for now.

Run `instrumentation --offline` to run all tests.

## Online tests

Local tests running on a private development network are meant to test the good
behavior in case of extreme situations but do not attest our capacity to handle
real network behavior.

First, follow a normal setup for the adapter and then run `instrumentation --online`. The
tested blockchain will be determined based on the taler configuration.

### Temporary database

If you do not want to use a persistent database for instrumentation tests, there
is a [script](../script/tmp_db.sh) to generate a temporary database similar to
local tests.