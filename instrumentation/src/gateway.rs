/*
  This file is part of TALER
  Copyright (C) 2022-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use std::str::FromStr;

use btc_wire::taler_utils::btc_payto_url;
use common::{
    api_common::{Amount as TalerAmount, Base32},
    api_wire::TransferRequest,
    rand_slice,
    url::Url,
};
use libdeflater::{CompressionLvl, Compressor};
use ureq::Response;

use crate::{
    btc::BtcCtx,
    utils::{cmd_out, cmd_redirect_ok, gateway_error, TestCtx},
};

fn client_transfer(gateway_url: &str, payto_url: &str, amount: &str) -> String {
    cmd_out(
        "taler-exchange-wire-gateway-client",
        &["-b", gateway_url, "-C", payto_url, "-a", amount],
    )
}

fn http_code(response: Result<Response, ureq::Error>) -> u16 {
    match response {
        Ok(resp) => resp.status(),
        Err(err) => match err {
            ureq::Error::Status(err, _) => err,
            ureq::Error::Transport(_) => unreachable!(),
        },
    }
}

/// Test wire-gateway conformance to documentation and its security
pub fn api(ctx: TestCtx) {
    ctx.step("Setup");
    let ctx = BtcCtx::setup(&ctx, "taler_btc.conf", false);

    ctx.step("Gateway API");
    {
        // Perform debits
        let mut amounts = Vec::new();
        for n in 1..10 {
            let amount = format!("{}:0.000{}", ctx.taler_conf.currency.to_str(), n);
            cmd_out(
                "taler-exchange-wire-gateway-client",
                &[
                    "-b",
                    &ctx.gateway_url,
                    "-D",
                    btc_payto_url(&ctx.client_addr).as_ref(),
                    "-a",
                    &amount,
                ],
            );
            amounts.push(amount);
        }

        // Check history
        let result = cmd_out(
            "taler-exchange-wire-gateway-client",
            &["-b", &ctx.gateway_url, "-i"],
        );
        for amount in &amounts {
            assert!(result.contains(amount));
        }

        // Perform credits
        let mut amounts = Vec::new();
        for n in 1..10 {
            let amount = format!("{}:0.0000{}", ctx.taler_conf.currency.to_str(), n);
            client_transfer(
                &ctx.gateway_url,
                btc_payto_url(&ctx.client_addr).as_ref(),
                &amount,
            );
            amounts.push(amount);
        }

        // Check history
        let result = cmd_out(
            "taler-exchange-wire-gateway-client",
            &["-b", &ctx.gateway_url, "-o"],
        );
        for amount in &amounts {
            assert!(result.contains(amount));
        }
    };

    ctx.step("Endpoint & Method");
    {
        // Unknown endpoint
        gateway_error(&format!("{}test", ctx.gateway_url), 404);
        // Method not allowed
        gateway_error(&format!("{}transfer", ctx.gateway_url), 405);
    }

    let amount = &format!("{}:0.00042", ctx.taler_conf.currency.to_str());
    let payto = btc_payto_url(&ctx.client_addr).to_string();

    ctx.step("Request format");
    {
        // Bad payto_url
        for url in [
            "http://bitcoin/$CLIENT",
            "payto://btc/$CLIENT",
            "payto://bitcoin/$CLIENT?id=admin",
            "payto://bitcoin/$CLIENT#admin",
            "payto://bitcoin/42$CLIENT",
        ] {
            let url = url.replace("$CLIENT", &ctx.client_addr.to_string());
            let result = client_transfer(&ctx.gateway_url, &url, amount);
            assert!(result.contains("(400/24)"));
        }

        // Bad transaction amount
        let result = client_transfer(&ctx.gateway_url, &payto, "ATC:0.00042");
        assert!(result.contains("(400/26)"));

        // Bad history delta
        for delta in [
            "incoming",
            "outgoing",
            "incoming?delta=0",
            "outgoing?delta=0;",
        ] {
            let code =
                http_code(ureq::get(&format!("{}history/{}", ctx.gateway_url, delta)).call());
            assert_eq!(code, 400);
        }
    }

    ctx.step("Transfer idempotence");
    {
        let mut request = TransferRequest {
            request_uid: Base32::from(rand_slice()),
            amount: TalerAmount::from_str(amount).unwrap(),
            exchange_base_url: ctx.taler_conf.base_url(),
            wtid: Base32::from(rand_slice()),
            credit_account: Url::from_str(&payto).unwrap(),
        };
        // Same
        assert_eq!(
            http_code(ureq::post(&format!("{}transfer", ctx.gateway_url)).send_json(&request)),
            200
        );
        assert_eq!(
            http_code(ureq::post(&format!("{}transfer", ctx.gateway_url)).send_json(&request)),
            200
        );
        // Collision
        request.amount.fraction += 42;
        assert_eq!(
            http_code(ureq::post(&format!("{}transfer", ctx.gateway_url)).send_json(&request)),
            409
        );
    }

    ctx.step("Security");
    {
        let big_hello: String = (0..1000).map(|_| "Hello_world").collect();
        // Huge body
        assert_eq!(
            http_code(ureq::post(&format!("{}transfer", ctx.gateway_url)).send_json(&big_hello)),
            400
        );

        // Body length liar
        assert_eq!(
            http_code(
                ureq::post(&format!("{}transfer", ctx.gateway_url))
                    .set("Content-Length", "1024")
                    .send_json(&big_hello)
            ),
            400
        );

        // Compression bomb
        let mut compressor = Compressor::new(CompressionLvl::best());
        let mut compressed = vec![0u8; compressor.deflate_compress_bound(big_hello.len())];
        let size = compressor
            .deflate_compress(big_hello.as_bytes(), &mut compressed)
            .unwrap();
        compressed.resize(size, 0);
        assert_eq!(
            http_code(
                ureq::post(&format!("{}transfer", ctx.gateway_url))
                    .set("Content-Encoding", "deflate")
                    .send_bytes(&compressed)
            ),
            400
        );
    }
}

/// Check btc-wire and wire-gateway correctly stop when a lifetime limit is configured
pub fn auth(ctx: TestCtx) {
    ctx.step("Setup");
    let ctx = BtcCtx::setup(&ctx, "taler_btc_auth.conf", false);

    ctx.step("Authentication");

    // No auth
    assert_eq!(
        http_code(ureq::get(&format!("{}history/outgoing", ctx.gateway_url)).call()),
        401
    );

    // Auth
    cmd_redirect_ok(
        "taler-exchange-wire-gateway-client",
        &[
            "--config",
            ctx.conf.to_str().unwrap(),
            "-s",
            "exchange-accountcredentials-admin",
            "-C",
            btc_payto_url(&ctx.client_addr).as_ref(),
            "-a",
            &format!("{}:0.00042", ctx.taler_conf.currency.to_str()),
        ],
        ctx.log("client"),
        "",
    );
}
