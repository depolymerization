/*
  This file is part of TALER
  Copyright (C) 2022-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use std::{
    panic::catch_unwind,
    path::PathBuf,
    sync::{Arc, Mutex},
    time::{Duration, Instant},
};

use clap::Parser;
use color_backtrace::termcolor::NoColor;
use common::{config::TalerConfig, currency::Currency};
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use owo_colors::OwoColorize;
use thread_local_panic_hook::set_hook;
use utils::TestDb;

use crate::utils::{try_cmd_redirect, TestCtx};

mod btc;
mod eth;
mod gateway;
mod utils;

/// Depolymerizer instrumentation test
#[derive(clap::Parser, Debug)]
struct Args {
    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(clap::Subcommand, Debug)]
enum Cmd {
    /// Perform online tests on running blockchain
    Online {
        /// Override default configuration file path
        #[clap(global = true, short, long)]
        config: Option<PathBuf>,
    },
    /// Perform offline tests on local private blockchain
    Offline {
        /// With tests to run
        #[clap(global = true, default_value = "")]
        filters: Vec<String>,
    },
}

pub fn main() {
    common::log::init();
    color_backtrace::install();

    let args = Args::parse();
    match args.cmd {
        Cmd::Online { config } => {
            let taler_config = TalerConfig::load(config.as_deref());
            let base_url = format!("http://localhost:{}/", taler_config.port());

            match taler_config.currency {
                Currency::BTC(_) => btc::online_test(config.as_deref(), &base_url),
                Currency::ETH(_) => eth::online_test(config.as_deref(), &base_url),
            }
            println!("Instrumentation test successful");
        }
        Cmd::Offline { filters } => {
            std::fs::remove_dir_all("log").ok();
            std::fs::create_dir_all("log/bin").unwrap();

            // Build binaries
            let p = ProgressBar::new_spinner();
            p.set_style(ProgressStyle::with_template("building {msg} {elapsed:.dim}").unwrap());
            p.enable_steady_tick(Duration::from_millis(1000));
            build_bin(&p, "wire-gateway", Some("test"), "wire-gateway");
            for name in ["btc-wire", "eth-wire"] {
                build_bin(&p, name, None, name);
                build_bin(&p, name, Some("fail"), &format!("{name}-fail"));
            }
            p.finish_and_clear();

            // Generate password
            let pwd: String = (0..30).map(|_| fastrand::alphanumeric()).collect();
            std::env::set_var("PASSWORD", pwd);

            // Run tests
            let m = MultiProgress::new();
            let start_style =
                &ProgressStyle::with_template("{prefix:.magenta} {msg} {elapsed:.dim}").unwrap();
            let ok_style =
                &ProgressStyle::with_template("{prefix:.magenta} {msg:.green} {elapsed:.dim}")
                    .unwrap();
            let err_style =
                &ProgressStyle::with_template("{prefix:.magenta} {msg:.red} {elapsed:.dim}")
                    .unwrap();

            let start = Instant::now();
            let db = Arc::new(TestDb::new());
            let results: Vec<_> = std::thread::scope(|s| {
                let tests: Vec<_> = TESTS
                    .iter()
                    .filter(|(_, name)| {
                        filters.is_empty() || filters.iter().any(|f| name.starts_with(f))
                    })
                    .map(|(action, name)| {
                        let pb = m.add(ProgressBar::new_spinner());
                        pb.set_style(start_style.clone());
                        pb.set_prefix(*name);
                        pb.set_message("Init");
                        pb.enable_steady_tick(Duration::from_millis(1000));
                        let db = db.clone();
                        let join = s.spawn(move || {
                            let start = Instant::now();
                            let ctx: TestCtx = TestCtx::new(name, pb.clone(), db);
                            let out = Arc::new(Mutex::new(String::new()));
                            let tmp = out.clone();
                            set_hook(Box::new(move |info| {
                                let mut buf = Vec::new();
                                color_backtrace::BacktracePrinter::new()
                                    .print_panic_info(info, &mut NoColor::new(&mut buf))
                                    .ok();
                                let str = String::from_utf8(buf).unwrap_or_default();
                                tmp.lock().unwrap().push_str(&str);
                            }));
                            let tmp = ctx.clone();
                            let result = catch_unwind(|| {
                                action(tmp);
                            });
                            if result.is_ok() {
                                pb.set_style(ok_style.clone());
                                pb.finish_with_message("OK");
                            } else {
                                pb.set_style(err_style.clone());
                                pb.finish();
                            }
                            let out: String = out.lock().unwrap().clone();
                            (result, start.elapsed(), out, pb.message())
                        });
                        (join, name)
                    })
                    .collect();
                tests
                    .into_iter()
                    .map(|(j, n)| (j.join().unwrap(), n))
                    .collect()
            });

            let len = results.len();
            m.clear().unwrap();
            for ((result, _, out, msg), name) in &results {
                if result.is_err() {
                    println!("{} {}\n{}", name.magenta(), msg.red(), out.bright_black());
                }
            }
            for ((result, time, _, msg), name) in results {
                match result {
                    Ok(_) => {
                        println!(
                            "{} {} {}",
                            name.magenta(),
                            "OK".green(),
                            format_args!("{}s", time.as_secs()).bright_black()
                        );
                    }
                    Err(_) => {
                        println!(
                            "{} {} {}",
                            name.magenta(),
                            msg.red(),
                            format_args!("{}s", time.as_secs()).bright_black()
                        );
                    }
                }
            }
            println!("{} tests in {}s", len, start.elapsed().as_secs());
        }
    }
}

pub fn build_bin(p: &ProgressBar, name: &str, features: Option<&str>, bin_name: &str) {
    p.set_message(bin_name.to_string());
    let mut args = vec!["build", "--bin", name, "--release"];
    if let Some(features) = features {
        args.extend_from_slice(&["--features", features]);
    }
    let result = try_cmd_redirect("cargo", &args, "log/bin/build")
        .unwrap()
        .0
        .wait()
        .unwrap();
    assert!(result.success());
    std::fs::rename(
        format!("target/release/{name}"),
        format!("log/bin/{bin_name}"),
    )
    .unwrap();
}

pub const TESTS: &[(fn(TestCtx), &str)] = &[
    (gateway::api, "gateway_api"),
    (gateway::auth, "gateway_auth"),
    (btc::wire, "btc_wire"),
    (btc::lifetime, "btc_lifetime"),
    (btc::reconnect, "btc_reconnect"),
    (btc::stress, "btc_stress"),
    (btc::conflict, "btc_conflict"),
    (btc::reorg, "btc_reorg"),
    (btc::hell, "btc_hell"),
    (btc::analysis, "btc_analysis"),
    (btc::bumpfee, "btc_bumpfee"),
    (btc::maxfee, "btc_maxfee"),
    (btc::config, "btc_config"),
    (eth::wire, "eth_wire"),
    (eth::lifetime, "eth_lifetime"),
    (eth::reconnect, "eth_reconnect"),
    (eth::stress, "eth_stress"),
    (eth::reorg, "eth_reorg"),
    (eth::hell, "eth_hell"),
    (eth::analysis, "eth_analysis"),
    (eth::bumpfee, "eth_bumpfee"),
    (eth::maxfee, "eth_maxfee"),
];
