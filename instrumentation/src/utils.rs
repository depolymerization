/*
  This file is part of TALER
  Copyright (C) 2022-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

use std::{
    fmt::Display,
    io::Write as _,
    net::{Ipv4Addr, SocketAddrV4, TcpListener, TcpStream},
    ops::{Deref, DerefMut},
    path::{Path, PathBuf},
    process::{Child, Command, Stdio},
    str::FromStr,
    sync::Arc,
    thread::sleep,
    time::{Duration, Instant},
};

use common::{
    api_common::{Amount, Base32},
    api_wire::{IncomingBankTransaction, IncomingHistory, OutgoingHistory, TransferRequest},
    config::TalerConfig,
    rand_slice,
    url::Url,
};
use indicatif::ProgressBar;
use tempfile::TempDir;

pub fn print_now(disp: impl Display) {
    print!("{}", disp);
    std::io::stdout().flush().unwrap();
}

#[must_use]
pub fn check_incoming(base_url: &str, txs: &[([u8; 32], Amount)]) -> bool {
    let res = ureq::get(&format!("{}history/incoming", base_url))
        .query("delta", &format!("-{}", txs.len()))
        .call()
        .unwrap();
    if txs.is_empty() {
        res.status() == 204
    } else {
        if res.status() != 200 {
            return false;
        }
        let history: IncomingHistory = res.into_json().unwrap();

        history.incoming_transactions.len() == txs.len()
            && txs.iter().all(|(reserve_pub_key, taler_amount)| {
                history.incoming_transactions.iter().any(|h| {
                matches!(
                    h,
                    IncomingBankTransaction::IncomingReserveTransaction {
                        reserve_pub,
                        amount,
                        ..
                    } if reserve_pub == &Base32::from(*reserve_pub_key) && amount == taler_amount
                )
            })
            })
    }
}

pub fn gateway_error(path: &str, error: u16) {
    let err = ureq::get(path).call().unwrap_err();
    match err {
        ureq::Error::Status(nb, _) => assert_eq!(nb, error),
        ureq::Error::Transport(_) => unreachable!(),
    }
}

#[must_use]
pub fn check_gateway_error(base_url: &str) -> bool {
    matches!(
        ureq::get(&format!("{}history/incoming", base_url))
            .query("delta", "-5")
            .call(),
        Err(ureq::Error::Status(504, _))
    )
}

#[must_use]
pub fn check_gateway_down(base_url: &str) -> bool {
    matches!(
        ureq::get(&format!("{}history/incoming", base_url))
            .query("delta", "-5")
            .call(),
        Err(ureq::Error::Status(502, _))
    )
}

#[must_use]
pub fn check_gateway_up(base_url: &str) -> bool {
    ureq::get(&format!("{}config", base_url)).call().is_ok()
}

pub fn transfer(base_url: &str, wtid: &[u8; 32], url: &Url, credit_account: Url, amount: &Amount) {
    ureq::post(&format!("{}transfer", base_url))
        .send_json(TransferRequest {
            request_uid: Base32::from(rand_slice()),
            amount: amount.clone(),
            exchange_base_url: url.clone(),
            wtid: Base32::from(*wtid),
            credit_account,
        })
        .unwrap();
}

#[must_use]
pub fn check_outgoing(base_url: &str, url: &Url, txs: &[([u8; 32], Amount)]) -> bool {
    let res = ureq::get(&format!("{}history/outgoing", base_url))
        .query("delta", &format!("-{}", txs.len()))
        .call()
        .unwrap();
    if txs.is_empty() {
        res.status() == 204
    } else {
        if res.status() != 200 {
            return false;
        }
        let history: OutgoingHistory = res.into_json().unwrap();

        history.outgoing_transactions.len() == txs.len()
            && txs.iter().all(|(wtid, amount)| {
                history.outgoing_transactions.iter().any(|h| {
                    h.wtid == Base32::from(*wtid)
                        && &h.exchange_base_url == url
                        && &h.amount == amount
                })
            })
    }
}
pub struct ChildGuard(pub Child);

impl Drop for ChildGuard {
    fn drop(&mut self) {
        self.0.kill().ok();
    }
}

#[track_caller]
pub fn cmd_out(cmd: &str, args: &[&str]) -> String {
    let output = Command::new(cmd)
        .args(args)
        .stdin(Stdio::null())
        .output()
        .unwrap();
    if output.stdout.is_empty() {
        String::from_utf8(output.stderr).unwrap()
    } else {
        String::from_utf8(output.stdout).unwrap()
    }
}

#[track_caller]
pub fn try_cmd_redirect(
    cmd: &str,
    args: &[&str],
    path: impl AsRef<Path>,
) -> std::io::Result<ChildGuard> {
    let log_file = std::fs::OpenOptions::new()
        .create(true)
        .append(true)
        .open(path)?;

    let child = Command::new(cmd)
        .args(args)
        .stderr(log_file.try_clone()?)
        .stdout(log_file)
        .stdin(Stdio::null())
        .spawn()?;
    Ok(ChildGuard(child))
}

#[track_caller]
pub fn cmd_redirect(cmd: &str, args: &[&str], path: impl AsRef<Path>) -> ChildGuard {
    try_cmd_redirect(cmd, args, path).unwrap()
}

#[track_caller]
pub fn cmd_ok(mut child: ChildGuard, name: &str) {
    let result = child.0.wait().unwrap();
    if !result.success() {
        panic!("cmd {} failed", name);
    }
}

#[track_caller]
pub fn cmd_redirect_ok(cmd: &str, args: &[&str], path: impl AsRef<Path>, name: &str) {
    cmd_ok(cmd_redirect(cmd, args, path), name)
}

#[track_caller]
pub fn retry_opt<T>(mut lambda: impl FnMut() -> Option<T>) -> T {
    let start = Instant::now();
    loop {
        let result = lambda();
        if result.is_none() && start.elapsed() < Duration::from_secs(60) {
            sleep(Duration::from_millis(500));
        } else {
            return result.unwrap();
        }
    }
}

#[track_caller]
pub fn retry(mut lambda: impl FnMut() -> bool) {
    retry_opt(|| lambda().then_some(()))
}

#[derive(Clone)]
pub struct TestCtx {
    pub name: String,
    pub log_dir: PathBuf,
    pub pb: ProgressBar,
    pub db: Arc<TestDb>,
}

impl TestCtx {
    pub fn new(name: &str, pb: ProgressBar, db: Arc<TestDb>) -> Self {
        // Create log dir
        let log_dir = PathBuf::from_str("log").unwrap().join(name);
        std::fs::remove_dir_all(&log_dir).ok();
        std::fs::create_dir_all(&log_dir).unwrap();

        Self {
            name: name.to_owned(),
            log_dir,
            pb,
            db,
        }
    }

    pub fn log(&self, name: &str) -> PathBuf {
        self.log_dir.join(format!("{name}.log"))
    }

    pub fn step(&self, disp: impl Display) {
        self.pb.set_message(format!("{disp}"))
    }

    /* ----- Database ----- */

    pub fn stop_db(&mut self) {
        self.db.stop_db(&self.name);
    }

    pub fn resume_db(&mut self) {
        self.db.resume_db(&self.name);
    }
}

pub struct TalerCtx {
    pub dir: TempDir,
    pub wire_dir: PathBuf,
    pub wire2_dir: PathBuf,
    pub conf: PathBuf,
    pub taler_conf: TalerConfig,
    ctx: TestCtx,
    pub wire_bin_path: String,
    stressed: bool,
    gateway: Option<ChildGuard>,
    pub gateway_url: String,
    wire: Option<ChildGuard>,
    wire2: Option<ChildGuard>,
    pub gateway_port: u16,
}

impl TalerCtx {
    pub fn new(ctx: &TestCtx, wire_name: impl Into<String>, config: &str, stressed: bool) -> Self {
        // Create temporary dir
        let dir = TempDir::new().unwrap();
        let conf = dir.path().join("taler.conf");

        // Create common dirs
        let wire_dir = dir.path().join("wire");
        let wire2_dir = dir.path().join("wire2");
        for dir in [&wire_dir, &wire2_dir] {
            std::fs::create_dir_all(dir).unwrap();
        }

        // Find unused port
        let gateway_port = unused_port();

        // Generate taler config from base
        let config = PathBuf::from_str("instrumentation/conf")
            .unwrap()
            .join(config);
        let mut config = ini::Ini::load_from_file(config).unwrap();
        let section = config
            .sections()
            .find(|it| {
                it.map(|it| it.starts_with("depolymerizer-"))
                    .unwrap_or(false)
            })
            .unwrap_or_default()
            .map(|it| it.to_string());
        config
            .with_section(Some("exchange-accountcredentials-admin"))
            .set(
                "WIRE_GATEWAY_URL",
                format!("http://localhost:{gateway_port}/"),
            );
        config
            .with_section(section)
            .set("CONF_PATH", wire_dir.to_string_lossy())
            .set("IPC_PATH", wire_dir.to_string_lossy())
            .set(
                "DB_URL",
                format!(
                    "postgres://localhost:{}/{}?user=postgres&password=password",
                    ctx.db.port, ctx.name
                ),
            )
            .set("PORT", gateway_port.to_string());

        config.write_to_file(&conf).unwrap();
        let taler_conf = TalerConfig::load(Some(&conf));

        let wire_name = wire_name.into();
        Self {
            dir,
            ctx: ctx.clone(),
            gateway_url: format!("http://localhost:{}/", taler_conf.port()),
            wire_dir,
            wire2_dir,
            conf,
            taler_conf,
            wire_bin_path: if stressed {
                format!("log/bin/{wire_name}-fail")
            } else {
                format!("log/bin/{wire_name}")
            },
            stressed,
            gateway: None,
            wire: None,
            wire2: None,
            gateway_port,
        }
    }

    pub fn init_db(&self) {
        self.db.create_db(&self.ctx.name);
        // Init db
        cmd_redirect_ok(
            &self.wire_bin_path,
            &["-c", self.conf.to_string_lossy().as_ref(), "initdb"],
            self.log("cmd"),
            "wire initdb",
        );
    }

    pub fn run(&mut self) {
        // Run gateway
        self.gateway = Some(cmd_redirect(
            "log/bin/wire-gateway",
            &["-c", self.conf.to_string_lossy().as_ref()],
            self.log("gateway"),
        ));

        // Start wires
        self.wire = Some(cmd_redirect(
            &self.wire_bin_path,
            &["-c", self.conf.to_string_lossy().as_ref()],
            self.log("wire"),
        ));
        self.wire2 = self.stressed.then(|| {
            cmd_redirect(
                &self.wire_bin_path,
                &["-c", self.conf.to_string_lossy().as_ref()],
                self.log("wire1"),
            )
        });

        // Wait for gateway to be up
        retry(|| {
            TcpStream::connect(SocketAddrV4::new(Ipv4Addr::LOCALHOST, self.gateway_port)).is_ok()
        });
    }

    /* ----- Process ----- */

    #[must_use]
    pub fn wire_running(&mut self) -> bool {
        self.wire.as_mut().unwrap().0.try_wait().unwrap().is_none()
    }

    #[must_use]
    pub fn gateway_running(&mut self) -> bool {
        self.gateway
            .as_mut()
            .unwrap()
            .0
            .try_wait()
            .unwrap()
            .is_none()
    }

    /* ----- Wire Gateway -----*/

    pub fn expect_credits(&self, txs: &[([u8; 32], Amount)]) {
        retry(|| check_incoming(&self.gateway_url, txs))
    }

    pub fn expect_debits(&self, base_url: &Url, txs: &[([u8; 32], Amount)]) {
        retry(|| check_outgoing(&self.gateway_url, base_url, txs))
    }

    pub fn expect_error(&self) {
        retry(|| check_gateway_error(&self.gateway_url));
    }

    pub fn expect_gateway_up(&self) {
        retry(|| check_gateway_up(&self.gateway_url));
    }

    pub fn expect_gateway_down(&self) {
        retry(|| check_gateway_down(&self.gateway_url));
    }
}

impl Deref for TalerCtx {
    type Target = TestCtx;

    fn deref(&self) -> &Self::Target {
        &self.ctx
    }
}

impl DerefMut for TalerCtx {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.ctx
    }
}

pub fn unused_port() -> u16 {
    TcpListener::bind(SocketAddrV4::new(Ipv4Addr::LOCALHOST, 0))
        .unwrap()
        .local_addr()
        .unwrap()
        .port()
}

pub struct TestDb {
    pub dir: TempDir,
    pub port: u16,
    _db: ChildGuard,
}

impl TestDb {
    pub fn new() -> Self {
        let dir = TempDir::new().unwrap();
        let port = unused_port();
        let log = "log/postgres.log";

        // Init databases files
        cmd_redirect_ok(
            "initdb",
            &[dir.path().to_string_lossy().as_ref()],
            log,
            "init_db",
        );
        // Generate database config
        std::fs::write(
            dir.path().join("postgresql.conf"),
            format!(
                "
                port={port}
                unix_socket_directories='{}'
                fsync=off
                synchronous_commit=off
                full_page_writes=off
                ",
                dir.path().to_string_lossy().as_ref()
            ),
        )
        .unwrap();
        let db = cmd_redirect(
            "postgres",
            &["-D", dir.path().to_string_lossy().as_ref()],
            log,
        );
        retry(|| {
            Self::execute_sql(
                port,
                "CREATE ROLE postgres LOGIN SUPERUSER PASSWORD 'password'",
            )
        });

        Self { dir, port, _db: db }
    }

    fn execute_sql(port: u16, sql: &str) -> bool {
        let mut psql = ChildGuard(
            Command::new("psql")
                .arg(format!("postgres://localhost:{}/postgres", port))
                .stderr(Stdio::null())
                .stdout(
                    std::fs::File::options()
                        .append(true)
                        .create(true)
                        .open("log/postgres.log")
                        .unwrap(),
                )
                .stdin(Stdio::piped())
                .spawn()
                .unwrap(),
        );
        psql.0
            .stdin
            .as_mut()
            .unwrap()
            .write_all(sql.as_bytes())
            .unwrap();
        psql.0.wait().unwrap().success()
    }

    pub fn create_db(&self, name: &str) {
        Self::execute_sql(
            self.port,
            &format!(
                "
                DROP DATABASE IF EXISTS {name};
                CREATE DATABASE {name};
                "
            ),
        );
    }

    pub fn stop_db(&self, name: &str) {
        Self::execute_sql(
            self.port,
            &format!(
                "
                UPDATE pg_database SET datallowconn=false WHERE datname='{name}';
                SELECT pg_terminate_backend(pid)
                FROM pg_stat_activity
                WHERE datname='{name}' AND pid <> pg_backend_pid();
                "
            ),
        );
    }
    pub fn resume_db(&self, name: &str) {
        Self::execute_sql(
            self.port,
            &format!("UPDATE pg_database SET datallowconn=true WHERE datname='{name}';"),
        );
    }
}
