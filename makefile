install:
	cargo install --path btc-wire --bin btc-wire
	cargo install --path eth-wire --bin eth-wire
	cargo install --path wire-gateway

segwit_demo: 
	cargo run --release --bin segwit-demo

test:
	RUST_BACKTRACE=full cargo run --profile dev --bin instrumentation -- offline

check:
	cargo check

msrv:
	cargo msrv --min 1.70.0