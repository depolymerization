#!/bin/bash

## Install required dependencies to run tests without sudo

set -eu

DIR=$(mktemp -d)

function cleanup() {
    for n in `jobs -p`; do
        kill $n 2> /dev/null || true
    done
    rm -rf $DIR 2> /dev/null
    wait
}

trap cleanup EXIT

echo "Ⅰ - Find installed postgres version"
PG_VER=`pg_config --version | egrep -o '[0-9]{1,}' | head -1`
echo "Found version $PG_VER"

echo "Ⅱ - Install bitcoind version 28.8"
cd $DIR
curl -L https://bitcoincore.org/bin/bitcoin-core-28.0/bitcoin-28.0-x86_64-linux-gnu.tar.gz -o btc.tar.gz
tar xvzf btc.tar.gz
rm -rfv ~/bitcoin
mkdir -pv ~/bitcoin
mv -v bitcoin-28.0/* ~/bitcoin

echo "Ⅲ - Install Go Ethereum (Geth) v1.13.5"
cd $DIR
curl -L https://gethstore.blob.core.windows.net/builds/geth-linux-amd64-1.13.5-916d6a44.tar.gz -o geth.tar.gz
tar xvzf geth.tar.gz
rm -rfv ~/geth
mkdir -pv ~/geth
mv -v geth-linux-amd64-1.13.5-916d6a44/* ~/geth

echo "Ⅴ - PATH"

echo "Add PATH=\"\$HOME/geth:\$HOME/bitcoin/bin:\$HOME/lnd:/usr/lib/postgresql/$PG_VER/bin:\$PATH\" to your bash profile"