#!/bin/bash

## Generate a new database in a temporary directory for instrumentation test against a local blockchain

source "${BASH_SOURCE%/*}/common.sh"

DIR=$(mktemp -d)

# Cleanup to run whenever we exit
function cleanup() {
    pg_ctl stop -D $DIR -w &> /dev/null
    wait
    rm -rf $DIR &> /dev/null || true
}

# Install cleanup handler (except for kill -9)
trap cleanup EXIT

echo  "----- Setup db -----"
pg_ctl init -D $DIR
echo "port=5454" >> $DIR/postgresql.conf
pg_ctl start -D $DIR -o "-c unix_socket_directories=$DIR"
echo "CREATE ROLE postgres LOGIN SUPERUSER PASSWORD 'password'" | psql -h localhost -p 5454 postgres > /dev/null
echo ""

read -p "Waiting for input to close DB:" IGNORE
