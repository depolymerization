# uri-pack

uri-pack is an efficient probabilistic binary encoding for URI

## Encoding

Most commonly used characters (a-z . / - %) are encoded using 5b, remaining
ASCII characters are encoded using 11b. If more than half the characters in a
URI are encoded with 5b, the encoded size is smaller than a simple ASCII format.

On the majestic_million database, 98.77% of the domain name were smaller, going
from an average encoded size of 14B in ASCII to 10B using our format.

## Usage

```rust
use uri_pack::{pack_uri, unpack_uri};

let domain = "http://example.com/static_file/image.png";
let encoded = pack_uri(domain).unwrap();
let decoded = unpack_uri(&encoded).unwrap();
assert_eq!(domain, decoded);
```
