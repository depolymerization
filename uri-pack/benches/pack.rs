/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion, Throughput};
use uri_pack::{pack_uri, unpack_uri};

/// Ascii char that can be packed into 5 bits
pub(crate) const PACKED: [u8; 30] = [
    b'a', b'b', b'c', b'd', b'e', b'f', b'g', b'h', b'i', b'j', b'k', b'l', b'm', b'n', b'o', b'p',
    b'q', b'r', b's', b't', b'u', b'v', b'w', b'x', b'y', b'z', b'.', b'/', b'-', b'%',
];

fn rand_compat(size: usize) -> String {
    String::from_utf8(
        std::iter::repeat_with(|| fastrand::u8(b'!'..=b'~'))
            .take(size)
            .collect(),
    )
    .unwrap()
}

fn rand_simple(size: usize) -> String {
    String::from_utf8(
        std::iter::repeat_with(|| PACKED[fastrand::usize(..PACKED.len())])
            .take(size)
            .collect(),
    )
    .unwrap()
}

fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("Uri");
    for size in [50, 500, 4048].iter() {
        group.throughput(Throughput::Bytes(*size as u64));
        group.bench_with_input(BenchmarkId::new("pack rand", size), size, |b, &size| {
            b.iter_batched(
                || rand_compat(size),
                |uri| pack_uri(&uri).unwrap(),
                criterion::BatchSize::SmallInput,
            )
        });
        group.bench_with_input(BenchmarkId::new("unpack rand", size), size, |b, &size| {
            b.iter_batched(
                || pack_uri(&rand_compat(size)).unwrap(),
                |packed| unpack_uri(&packed),
                criterion::BatchSize::SmallInput,
            )
        });
        group.bench_with_input(BenchmarkId::new("pack simple", size), size, |b, &size| {
            b.iter_batched(
                || rand_simple(size),
                |uri| pack_uri(&uri).unwrap(),
                criterion::BatchSize::SmallInput,
            )
        });
        group.bench_with_input(BenchmarkId::new("unpack simple", size), size, |b, &size| {
            b.iter_batched(
                || pack_uri(&rand_simple(size)).unwrap(),
                |packed| unpack_uri(&packed),
                criterion::BatchSize::SmallInput,
            )
        });
    }
    group.finish();
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
