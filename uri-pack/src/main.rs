/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use std::cmp::Ordering;

use uri_pack::pack_uri;

/// Compute efficiency statistics of uri-pack using domains from majestic_million

fn main() {
    let mut majestic = csv::Reader::from_reader(include_str!("majestic_million.csv").as_bytes());
    let mut ascii_counter = [0u64; 255];
    let mut before_len = 0;
    let mut after_len = 0;
    let mut bigger = 0;
    let mut same = 0;
    let mut smaller = 0;
    for record in majestic.records() {
        let domain = &record.unwrap()[2];
        for ascii in domain.as_bytes() {
            ascii_counter[*ascii as usize] += 1;
        }
        let before = domain.as_bytes().len();
        let after = pack_uri(domain).unwrap().len();
        before_len += before;
        after_len += after;
        match before.cmp(&after) {
            Ordering::Less => bigger += 1,
            Ordering::Equal => same += 1,
            Ordering::Greater => smaller += 1,
        }
    }
    let sum: u64 = ascii_counter.iter().sum();
    let max_len = ascii_counter.iter().max().unwrap_or(&0).to_string().len();
    for (ascii, count) in ascii_counter
        .into_iter()
        .enumerate()
        .filter(|(_, count)| *count > 0)
    {
        println!(
            "{} {:>4$} {:.2}% {:=<5$}",
            ascii as u8 as char,
            count,
            count as f32 / sum as f32 * 100.,
            "",
            max_len,
            (count * 200 / sum) as usize,
        )
    }
    let count = bigger + smaller + same;
    println!(
        "\nBefore ~{}b   After ~{}b\nBigger {:.2}%   Same {:.2}%   Smaller {:.2}%",
        before_len / count,
        after_len / count,
        (bigger as f32 / count as f32 * 100.),
        (same as f32 / count as f32 * 100.),
        (smaller as f32 / count as f32 * 100.)
    );
}
