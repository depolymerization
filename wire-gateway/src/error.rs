/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use common::{api_common::ErrorDetail, error_codes::ErrorCode};
use hyper::{header, Body, Response, StatusCode};

/// Generic http error
#[derive(Debug)]
pub struct ServerError {
    pub status: StatusCode,
    content: Content,
    pub msg: String,
}

#[derive(Debug)]
enum Content {
    None,
    Json(Vec<u8>),
}

impl ServerError {
    fn new(status: StatusCode, content: Content, msg: String) -> Self {
        Self {
            status,
            content,
            msg,
        }
    }

    pub fn response(self) -> (Response<Body>, String) {
        let builder = Response::builder().status(self.status);
        let result = match self.content {
            Content::None => builder.body(Body::empty()),
            Content::Json(it) => builder
                .header(header::CONTENT_TYPE, "application/json")
                .body(Body::from(it)),
        };
        (result.unwrap(), self.msg)
    }

    pub fn unexpected<E: std::error::Error>(e: E) -> Self {
        Self::new(
            StatusCode::INTERNAL_SERVER_ERROR,
            Content::None,
            format!("unexpected: {}", e),
        )
    }

    fn detail(status: StatusCode, code: ErrorCode, msg: String) -> Self {
        let detail = ErrorDetail {
            code: code as i64,
            hint: None,
            detail: None,
            parameter: None,
            path: None,
            offset: None,
            index: None,
            object: None,
            currency: None,
            type_expected: None,
            type_actual: None,
        };
        match serde_json::to_vec(&detail) {
            Ok(json) => Self::new(status, Content::Json(json), msg),
            Err(e) => Self::unexpected(e),
        }
    }

    pub fn status(status: StatusCode) -> Self {
        Self::new(
            status,
            Content::None,
            status.canonical_reason().unwrap_or("").to_string(),
        )
    }

    pub fn code(status: StatusCode, code: ErrorCode) -> Self {
        Self::detail(
            status,
            code,
            format!(
                "standard {}: {}",
                code as i64,
                status.canonical_reason().unwrap_or("")
            ),
        )
    }

    pub fn catch_code<E: std::error::Error>(e: E, status: StatusCode, code: ErrorCode) -> Self {
        Self::detail(status, code, format!("standard {}: {}", code as i64, e))
    }
}

impl From<tokio_postgres::Error> for ServerError {
    fn from(e: tokio_postgres::Error) -> Self {
        ServerError::catch_code(
            e,
            StatusCode::BAD_GATEWAY,
            ErrorCode::GENERIC_DB_FETCH_FAILED,
        )
    }
}
pub trait CatchResult<T: Sized> {
    fn unexpected(self) -> Result<T, ServerError>;
    fn catch_code(self, status: StatusCode, code: ErrorCode) -> Result<T, ServerError>;
}

impl<T, E: std::error::Error> CatchResult<T> for Result<T, E> {
    fn unexpected(self) -> Result<T, ServerError> {
        self.map_err(|e| ServerError::unexpected(e))
    }

    fn catch_code(self, status: StatusCode, code: ErrorCode) -> Result<T, ServerError> {
        self.map_err(|e| ServerError::catch_code(e, status, code))
    }
}
