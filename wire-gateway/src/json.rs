/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
use hyper::{body::HttpBody, header, http::request::Parts, Body, Response, StatusCode};
use miniz_oxide::inflate::TINFLStatus;

const MAX_PAYLOAD_SIZE: u64 = 4 * 1024; // 4kB

#[derive(Debug, thiserror::Error)]
pub enum ParseBodyError {
    #[error(transparent)]
    Body(#[from] hyper::Error),
    #[error(transparent)]
    Json(#[from] serde_json::Error),
    #[error("deflate decompression error")]
    Deflate,
    #[error("body is suspiciously big")]
    SuspiciousBody,
    #[error("decompression is suspiciously big")]
    SuspiciousCompression,
}

/// Parse json body, perform security check and decompression
pub async fn parse_body<J: serde::de::DeserializeOwned>(
    parts: &Parts,
    body: Body,
) -> Result<J, ParseBodyError> {
    // Check announced body size
    if body.size_hint().upper().unwrap_or(u64::MAX) > MAX_PAYLOAD_SIZE {
        return Err(ParseBodyError::SuspiciousBody);
    }
    // Read body
    let bytes = hyper::body::to_bytes(body).await?;

    // Decompress if necessary
    if parts
        .headers
        .get(header::CONTENT_ENCODING)
        .map(|it| it == "deflate")
        .unwrap_or(false)
    {
        let decompressed = miniz_oxide::inflate::decompress_to_vec_zlib_with_limit(
            &bytes,
            MAX_PAYLOAD_SIZE as usize,
        )
        .map_err(|s| match s.status {
            TINFLStatus::HasMoreOutput => ParseBodyError::SuspiciousCompression,
            _ => ParseBodyError::Deflate,
        })?;
        // Parse json
        Ok(serde_json::from_slice(&decompressed)?)
    } else {
        // Parse json
        Ok(serde_json::from_slice(&bytes)?)
    }
}

#[derive(Debug, thiserror::Error)]
pub enum EncodeBodyError {
    #[error(transparent)]
    Json(#[from] serde_json::Error),
}

pub fn encode_body<J: serde::Serialize>(
    parts: &Parts,
    status: StatusCode,
    json: &J,
) -> Result<Response<Body>, EncodeBodyError> {
    let json = serde_json::to_vec(json)?;
    if parts
        .headers
        .get(header::ACCEPT_ENCODING)
        .and_then(|it| it.to_str().ok())
        .map(|str| str.contains("deflate"))
        .unwrap_or(false)
    {
        let compressed = miniz_oxide::deflate::compress_to_vec_zlib(&json, 6);
        Ok(Response::builder()
            .status(status)
            .header(header::CONTENT_TYPE, "application/json")
            .header(header::CONTENT_ENCODING, "deflate")
            .body(Body::from(compressed))
            .unwrap())
    } else {
        Ok(Response::builder()
            .status(status)
            .header(header::CONTENT_TYPE, "application/json")
            .body(Body::from(json))
            .unwrap())
    }
}
